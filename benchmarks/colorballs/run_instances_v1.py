#!/usr/bin/env python3
import sys
import os
from run_cmd import run_command

def main(argv):
    if len(argv) == 0:
        argv.append(os.getcwd())

    if not os.path.isdir(argv[0]):
        raise ValueError("First argument must be output directory!")

    command = " -o " + argv[0]
    command += " -pl colorball_outcome_binary.dif "
    command += " -pd colorball_base.lp "
    command += " -pd colorball_outcome_binary.lp colorball_action_single.lp colorball_eq.lp "
    command += " -pi colorball_initial.lp "
    command += " --parallel 20 "
    command += " --visited_state_level 1 "

    run_command(argv,"colorball_v1",command)

if __name__ == '__main__':
    main(sys.argv[1:])