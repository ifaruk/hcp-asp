#!/usr/bin/env python3
import sys
import os
from run_cmd import run_command

def main(argv):
    if len(argv) == 0:
        argv.append(os.getcwd())

    if not os.path.isdir(argv[0]):
        raise ValueError("First argument must be output directory!")

    command = " -o " + argv[0]
    command += " -pl doors.dif "
    command += " -pd doors.lp "
    command += " -pi doors_initial.lp "
    command += " --parallel 20 "
    command += "  --visited_state_level 1 "

    run_command(argv,"doors_v0",command)

if __name__ == '__main__':
    main(sys.argv[1:])
    # try:
    #     main(sys.argv[1:])
    # except Exception as e:
    #     raise e
    #     import traceback, code
    #
    #     type, value, tb = sys.exc_info()
    #     traceback.print_exc()
    #     last_frame = lambda tb=tb: last_frame(tb.tb_next) if tb.tb_next else tb
    #     frame = last_frame().tb_frame
    #     ns = dict(frame.f_globals)
    #     ns.update(frame.f_locals)
    #     code.interact(local=ns)

