import subprocess
import datetime
from os import listdir
from os.path import isfile, join

def run_command(argv,folder_initial,command):

    command = "hcpasp --incremental_mode"+command
    #5 hour timeout
    timeout_sec = str(5*60*60)
    command += " -pl_time "+str(timeout_sec)+" -bl_time "+str(timeout_sec)+" "
    print("\n"*1)
    print("With "+timeout_sec+" sec. time out.")

    if len(argv)>1:
        if isfile(join("instances",argv[1])):
            all_instance_files = [argv[1]]
        else:
            raise Exception("File "+argv[1]+" not found under 'instances'.")
    else:
        all_instance_files = [f for f in listdir("instances") if isfile(join("instances", f))]
        all_instance_files.sort()

    for instance_file in all_instance_files:
        ifile_split = instance_file.split(".")
        if ifile_split[-1] != "lp":
            continue
        print("")
        prev_time=datetime.datetime.now()
        print("Current time: "+str(datetime.datetime.now()))
        print("Experiment: "+folder_initial)
        instance_file_name = instance_file.split(".")[0]
        execute_command = command + "-pd instances/"+instance_file +' -n '+folder_initial+"_"+instance_file_name
        print(execute_command)

        try:
            subprocess.Popen(execute_command, shell=True, stdout=subprocess.PIPE).stdout.read()
            # subprocess.Popen(execute_command, stdout=subprocess.PIPE,universal_newlines=True)
            # stdout_lines = iter(popen.stdout.readline, "")
            # for stdout_line in stdout_lines:
            #     print(stdout_line)
            # popen.stdout.close()
            # return_code = popen.wait()
        except Exception as e:
            print(e)

        print("Elapsed time: "+str(datetime.datetime.now()-prev_time))
