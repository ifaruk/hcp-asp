//
// Created by faruxx on 02.09.2016.
//


#include <asp_tools/asp_driver.h>
#include <utils.h>
#include <plan.h>

#include <boost/thread/shared_mutex.hpp>

#include <string>
#include <functional>
#include <numeric>
#include <tuple>
#include <unordered_set>
#include <unordered_map>
#include <type_traits>
#include <queue>
#include <atomic>
#include <thread>
#include <mutex>
#include <condition_variable>

#ifndef HCP_PLANNER_H
#define HCP_PLANNER_H

namespace HCP
{
	class Planner
	{
	public:
		
		struct Edge_explore
		{
			int32_t  depth;
			int32_t  priority;
			uint64_t lower_search_bound;
			Edge_sp  parent;
			bool     landmark;
			
			Edge_explore(int32_t depth,
			             int32_t priority,
			             uint64_t lower_search_bound,
			             bool landmark,
			             const Edge_sp& parent,
			             const Edge_sp& to_explore_edge);
			
			Edge_sp to_explore_edge;
		};
		
		typedef std::shared_ptr<Edge_explore> Edge_explore_sp;
		
		typedef std::vector<Edge_explore_sp> Edge_explore_sp_vector;
		
		typedef std::shared_ptr<Edge_explore_sp_vector> Edge_explore_sp_vector_sp;
		
		typedef std::tuple<std::shared_ptr<std::vector<std::string>>,
		                   int64_t,
		                   uint64_t> Find_plan_return;
		
		typedef std::tuple<bool,
		                   Edge_explore_sp_vector_sp> Create_path_return;
		
		enum Planner_state
		{
			PLANNER_STATE_NONE,
			PLANNER_STATE_STARTING,
			PLANNER_STATE_RUNNING,
			PLANNER_STATE_ENDED_SUCCESSFUL,
			PLANNER_STATE_ENDED_TIMEOUT,
		};
		
		/**Branch Computing Thread
		 *
		 */
		struct BCT
		{
			uint64_t thread_id;
			int64_t  millisecond_from_start;
			uint64_t time;
			uint64_t plan_length;
			Edge_sp  edge;
			bool     failed;
			
			bool
			operator<(const BCT& bct) const
			{
				return millisecond_from_start < bct.millisecond_from_start;
			}
		};
		
		Planner(Configuration_sp& conf);
		
		bool
		start_work(void);
		
		inline Edge_sp
		get_result(void) const
		{
			return solution_root;
		}
		
		inline Planner_state
		get_state(void) const
		{
			return cur_state;
		}
		
		void
		worker_thread(int thread_id);
		
		inline uint16_t
		get_number_of_threads(void) const
		{
			return branch_instance_no_threads;
		}
		
		inline std::vector<BCT> const&
		get_branch_computation_timing(void) const
		{
			return branch_computation_timing;
		}
		
		
		//@important tid start from 1. 0 means return for all threads.
		
		inline uint64_t
		get_cumulative_time_spend_on_processing(void) const
		{
			return static_cast<uint64_t>(std::accumulate(cumulative_time_spend_on_processing.begin(),
			                                             cumulative_time_spend_on_processing.begin() + branch_instance_no_threads,
			                                             0));
		}
		
		std::array<uint64_t,
		           MAX_THREAD_COUNT> const&
		get_cumulative_time_spend_on_processing_by_thread(void) const
		{
			return cumulative_time_spend_on_processing;
		}
		
		uint64_t
		get_total_number_of_clingo_execution(void) const
		{
			return static_cast<uint64_t>(std::accumulate(total_number_of_clingo_execution.begin(),
			                                             total_number_of_clingo_execution.begin() + branch_instance_no_threads,
			                                             0));
		}
		
		std::array<uint64_t,
		           MAX_THREAD_COUNT> const&
		get_total_number_of_clingo_execution_by_thread(void) const
		{
			return total_number_of_clingo_execution;
		}
		
		uint64_t
		get_total_clingo_execution_time(void) const
		{
			return static_cast<uint64_t>(std::accumulate(total_clingo_execution_time.begin(),
			                                             total_clingo_execution_time.begin() + branch_instance_no_threads,
			                                             0));
		}
		
		std::array<uint64_t,
		           MAX_THREAD_COUNT> const&
		get_total_clingo_execution_time_by_thread(void) const
		{
			return total_clingo_execution_time;
		}
		
		uint64_t
		get_min_clingo_execution_instance_time(void) const
		{
			
			return static_cast<uint64_t>(* std::min_element(min_clingo_execution_instance_time.begin(),
			                                                min_clingo_execution_instance_time.begin() + branch_instance_no_threads));
		}
		
		std::array<uint64_t,
		           MAX_THREAD_COUNT> const&
		get_min_clingo_execution_instance_time_by_thread(void) const
		{
			return min_clingo_execution_instance_time;
		}
		
		uint64_t
		get_max_clingo_execution_instance_time(void) const
		{
			return static_cast<uint64_t>(* std::max_element(max_clingo_execution_instance_time.begin(),
			                                                max_clingo_execution_instance_time.begin() + branch_instance_no_threads));
		}
		
		std::array<uint64_t,
		           MAX_THREAD_COUNT> const&
		get_max_clingo_execution_instance_time_by_thread(void) const
		{
			return max_clingo_execution_instance_time;
		}

		bool
		print_plan_tree();

	private:
		///Type definitions
		using clock_type = typename std::conditional<std::chrono::high_resolution_clock::is_steady,
		                                             std::chrono::high_resolution_clock,
		                                             std::chrono::steady_clock>::type;
		
		Configuration_sp      conf;
		std::atomic<uint64_t> next_branch_id;
		Planner_state         cur_state;
		
		//----------------------------------------------------------
		uint16_t const           branch_instance_no_threads;
		uint16_t const           clingo_instance_per_branch_no_threads;
		std::atomic<uint32_t>    clingo_instance_per_branch_adaptive_no_threads;//used to hold 0.1 unit value
		uint32_t const           clingo_instance_in_program_max;
		std::atomic<uint32_t>    clingo_instance_in_program_count;
		std::vector<std::string> common_command_head;
		
		std::vector<char const*> libclingo_args; //For libclingo_execute
		std::string              problem_compiled_user_data; //For libclingo_execute
		
		//----------------------------------------------------------
		Edge_sp                                             solution_root;
		std::vector<std::thread>                            thread_vector;
		std::unordered_map<std::thread::id,
		                   int>                             thread_id_to_index_map;
		std::vector<std::shared_ptr<ASP_tools::ASP_driver>> asp_parser_sp_vector;
		//----------------------------------------------------------
		std::mutex                                          print_mutex;
		//----------------------------------------------------------
		std::mutex                                          task_queue_mutex;
		//@warning: all following parameters must be used under protection of "task_queue_mutex".
		std::condition_variable                             task_queue_cv;
		
		static const std::string unsatisfiable;
		static const std::string satisfiable;
		static const std::string answer;
		static const std::string unknown;
		
		class Edge_explore_comparator
		{
		public:
			bool
			operator()(Planner::Edge_explore_sp const& one,
			           Planner::Edge_explore_sp const& two)
			{
				//return true if swap necessary, one is on top of two
				if(one->landmark && !two->landmark)
				{
					return true;
				}
				else if(one->landmark == two->landmark)
				{
					if(one->priority < two->priority)
					{
						return true;
					}
					else if(one->priority == two->priority)
					{
						if(one->depth > two->depth)
						{
							return true;
						}
					}
				}
				return false;
			}
		};
		
		std::priority_queue<Edge_explore_sp,
		                    Edge_explore_sp_vector,
		                    Edge_explore_comparator> task_queue;
		bool                                         planning_completed;
		uint16_t                                     working_threads;
		uint16_t                                     no_main_tasks_running;//tasks that are not landmakrs
		clock_type::time_point                       planner_start_time;
		
		//----------------------------------------------------------
		typedef std::unordered_set<ASP_tools::Function_sp> Timeless_state_function_set;
		Timeless_state_function_set                        timeless_state_function_set;
		boost::shared_mutex                                timeless_state_function_set_mutex;
		//---------------------------------------------------------
		typedef std::unordered_map<std::size_t,
		                           Edge_sp>                Visited_state_map;
		Visited_state_map                                  visited_states;
		boost::shared_mutex                                visited_states_mutex;
		//---------------------------------------------------------
		//Statistics Data
		
		///@warning values are in milliseconds
		alignas(CACHE_SIZE) std::array<uint64_t,
		                               MAX_THREAD_COUNT> cumulative_time_spend_on_processing;
		
		alignas(CACHE_SIZE) std::array<uint64_t,
		                               MAX_THREAD_COUNT> total_number_of_clingo_execution;
		alignas(CACHE_SIZE) std::array<uint64_t,
		                               MAX_THREAD_COUNT> total_clingo_execution_time;
		
		alignas(CACHE_SIZE) std::array<uint64_t,
		                               MAX_THREAD_COUNT> min_clingo_execution_instance_time;
		alignas(CACHE_SIZE) std::array<uint64_t,
		                               MAX_THREAD_COUNT> max_clingo_execution_instance_time;
		
		boost::shared_mutex bct_mutex;
		std::vector<BCT>    branch_computation_timing;
		
		//---------------------------------------------------------
		//Functions
		
		void
		print_message(std::string const& message);
		
		//----------------------------------------------------------
		
		
		std::shared_ptr<std::vector<std::string>>
		prepare_external_data(Edge_explore_sp const& edge_explore);
		
		void
		prepare_executed_command(std::string const branch_id,
		                         std::string command);
		
		//Function pointer to either 'clingo_execute' or 'libclingo execute'
		typedef std::shared_ptr<std::vector<std::string>>
		(Planner::*Solver_execute_function)(uint64_t const branch_id,
		                                    uint16_t const thread_id,
		                                    uint32_t const cur_step,
		                                    uint32_t const increase_step,
		                                    uint32_t const time_min,
		                                    std::string const ext,
		                                    uint32_t const time_limit);
		
		Solver_execute_function solver_execute_function;
		
		std::shared_ptr<std::vector<std::string>>
		clingo_execute(uint64_t const branch_id,
		               uint16_t const thread_id,
		               uint32_t const cur_step,
		               uint32_t const increase_step,
		               uint32_t const time_min,
		               std::string const ext_file_name,
		               uint32_t const time_limit);
		
		//@warning uncomment for libclingo
		//		std::shared_ptr<std::vector<std::string>>
		//		libclingo_execute(uint64_t const branch_id,
		//											uint16_t const thread_id,
		//											uint32_t const cur_step,
		//											uint32_t const increase_step,
		//											uint32_t const time_min,
		//											std::string const ext_data);
		
		Find_plan_return
		find_plan_with_clingo(Edge_explore_sp const& edge_explore,
		                      std::shared_ptr<std::vector<std::string>>& ext_data);
		
		History_sp
		prepare_history(Edge_sp const& root_edge,
		                std::shared_ptr<std::vector<std::string>> const& solution);
		
		Create_path_return
		create_path(Edge_explore_sp root_edge,
		            History_sp history);
		
		Edge_explore_sp_vector_sp
		branch_planner(Edge_explore_sp& edge_explore_tuple);
	};
}
#endif //HCP_PLANNER_H
