//
// Created by faruxx on 24.08.2016.
//

#ifndef CSOFTWARE_ASP_driver_H
#define CSOFTWARE_ASP_driver_H

#include <string>
#include <vector>

#include "asp_tools.h"
#include "asp_scanner.h"
#include <parser/asp_parser.h>


namespace ASP_tools
{
	class ASP_driver
	{
	public:
		/// construct a new parser driver context
		ASP_driver();
		
		
		/** Invoke the scanner and parser on an input string.
		 * @param input	input string
		 * @param sname	stream name for error messages
		 * @return		true if successfully parsed
		 */
		bool
		parse_string(const std::string& input,
		             const std::string& sname = "string stream");
		
		bool
		parse_stream(std::istream& in,
		             const std::string& sname);
		
		void
		clear(void);
		
		
		Term_sp
		get_outcome(void);
		
		// To demonstrate pure handling of parse errors, instead of
		// simply dumping them on the standard error output, we will pass
		// them to the driver using the following two member functions.
		
		/** Error handling with associated line number. This can be modified to
		 * output the error e.g. to a dialog box. */
		void
		error(const class location& l,
		      const std::string& m);
		
		friend class ASP_parser;
		
		friend class ASP_scanner;
	
	private:
		// Used internally by Scanner YY_USER_ACTION to update location indicator
		void
		increaseLocation(unsigned int loc);
		
		// Used to get last Scanner location. Used in error messages.
		unsigned int
		location() const;
	
	private:
		ASP_scanner m_scanner;
		ASP_parser  m_parser;
		
		Term_sp result;
		
		unsigned int m_location;
	};
}

#endif //CSOFTWARE_ASP_driver_H
