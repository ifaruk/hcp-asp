//
// Created by faruxx on 24.08.2016.
//

#ifndef CSOFTWARE_ASP_SCANNER_H
#define CSOFTWARE_ASP_SCANNER_H

#ifndef YY_DECL

#define  YY_DECL            \
    ASP_tools::ASP_parser::token_type        \
    ASP_tools::ASP_scanner::lex(ASP_driver& driver)
#endif


#ifndef __FLEX_LEXER_H
#define yyFlexLexer ASP_FlexLexer

#include "FlexLexer.h"

#undef yyFlexLexer
#endif


// Scanner method signature is defined by this macro. Original yylex() returns int.
// Sinice Bison 3 uses symbol_type, we must change returned type. We also rename it
// to something sane, since you cannot overload return type.
#undef YY_DECL
#define YY_DECL ASP_tools::ASP_parser::symbol_type ASP_tools::ASP_scanner::get_next_token()


#include <parser/asp_parser.h>

namespace ASP_tools
{
	
	class ASP_scanner: public ASP_FlexLexer
	{
	public:
		/** Create a new scanner object. The streams arg_yyin and arg_yyout default
			 * to cin and cout, but that assignment is only made when initializing in
			 * yylex(). */
		ASP_scanner(ASP_driver& driver);
		
		/** Required for virtual functions */
		virtual ~ASP_scanner();
		
		virtual ASP_parser::symbol_type
		get_next_token();
	
	private:
		ASP_driver& m_driver;
	};
}

#endif //CSOFTWARE_ASP_SCANNER_H
