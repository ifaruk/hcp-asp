//
// Created by faruxx on 18.08.2016.
//

#ifndef CSOFTWARE_ASP_ATOM_PARSER_H
#define CSOFTWARE_ASP_ATOM_PARSER_H

#include <map>
#include <vector>
#include <string>
#include <memory>
#include <unordered_map>
#include <functional>

namespace ASP_tools
{
	
	class parser;
	
	/*------------------*/
	
	class Term;
	
	class Function;
	
	class Tuple;
	
	class Variable;
	
	class Identifier;
	
	class Number;
	
	typedef Term      * Term_p;
	typedef Function  * Function_p;
	typedef Tuple     * Tuple_p;
	typedef Variable  * Variable_p;
	typedef Identifier* Identifier_p;
	typedef Number    * Number_p;
	
	
	typedef std::shared_ptr<Term>       Term_sp;
	typedef std::shared_ptr<Function>   Function_sp;
	typedef std::shared_ptr<Tuple>      Tuple_sp;
	typedef std::shared_ptr<Variable>   Variable_sp;
	typedef std::shared_ptr<Identifier> Identifier_sp;
	typedef std::shared_ptr<Number>     Number_sp;
	
	typedef std::vector<Function_sp> Function_sp_vector;
	typedef std::vector<Term_sp>     Term_sp_vector;
	
	
	struct Instance_mapping_comparator;
	struct Instance_mapping_hasher;
	
	typedef std::unordered_map<Variable_sp,
	                           Term_sp,
	                           Instance_mapping_hasher,
	                           Instance_mapping_comparator> Instance_variable_map;
	
	typedef std::pair<Variable_sp,
	                  Term_sp> Instance_single_map;
	
	typedef std::shared_ptr<Instance_variable_map> Instance_variable_map_sp;
	
	typedef std::pair<bool,
	                  Instance_variable_map_sp> Instance_return;
	
	
	struct Instance_mapping_hasher
	{
		std::size_t
		operator()(Variable_sp const& a) const;
	};
	
	struct Instance_mapping_comparator
	{
		bool
		operator()(Variable_sp const& a,
		           Variable_sp const& b) const;
	};
	
	class Term
	{
	protected:
		std::string print_str;
	public:
		enum Term_type
		{
			NONE, FUN, TUP, VAR, IDE, NUM,
		};
		
		Term_type type;
		
		Term(Term_type type_ = Term_type::NONE);
		
		virtual ~Term()
		{
		};
		
		virtual Term_sp
		clone() const =0;
		
		virtual bool
		operator==(const Term& other) const =0;
		
		virtual bool
		operator!=(const Term& other) const =0;
		
		virtual inline bool
		operator()(const Term& other) const
		{
			return print_str < other.print_str;
		};
		
		virtual inline std::string const&
		to_string() const
		{
			return print_str;
		};
		
		virtual inline void
		set_string()=0;
		
		virtual Instance_return
		get_variable_map(Term_sp const& other) const =0;
		
		virtual bool
		check_instance(Term_sp const& other) const =0;
		
		virtual Term_sp
		set_member(Instance_variable_map_sp const& map) const =0;
	};
	
	template<class Derived>
	class Term_derivation_helper: public Term
	{
	public:
		Term_derivation_helper(Term_type type)
			: Term(type)
		{};
		
		virtual Term_sp
		clone() const
		{
			return std::static_pointer_cast<Term>(std::make_shared<Derived>(static_cast<const Derived&>(* this))); // call the copy ctor.
		}
	};
	
	//Term
	//----------------------------------------------------------------------
	//----------------------------------------------------------------------
	//Function
	
	
	
	class Function: public Term_derivation_helper<Function>
	{
	private:
		Identifier_sp  name;
		Term_sp_vector args;
		bool           neg;
		
		uint16_t                     no_args;
		std::shared_ptr<std::string> name_str;
	public:
		
		Function(Identifier_sp name_,
		         bool neg_ = false);
		
		Function(Identifier_sp name_,
		         Term_sp_vector const& args_,
		         bool neg_ = false);
		
		virtual ~Function()
		{
		};
		
		std::shared_ptr<std::string>
		get_name(void) const
		{
			return name_str;
		}
		
		Function_sp
		negative(void)
		{
			return std::make_shared<Function>(name,
			                                  args,
			                                  !neg);
		}
		
		Term_sp_vector const&
		get_args(void) const;
		
		uint16_t
		get_no_args(void) const;
		
		/*-----------------------------------------------------------------*/
		void
		set_string(void);
		
		virtual Instance_return
		get_variable_map(Term_sp const& other) const;
		
		virtual bool
		check_instance(Term_sp const& other) const;
		
		virtual Term_sp
		set_member(Instance_variable_map_sp const& map) const;
		
		/*-----------------------------------------------------------------*/
		/*Operator overloads*/
		
		virtual bool
		operator==(Function const& other) const;
		
		virtual inline bool
		operator!=(Function const& other) const
		{
			return !operator==(other);
		}
		
		virtual inline bool
		operator==(Term const& other) const
		{
			return type != other.type ? false : operator==(dynamic_cast<Function const&>(other));
		}
		
		virtual inline bool
		operator!=(const Term& other) const
		{
			return !operator==(other);
		}
	};
	
	
	//FUNCTION
	//------------------------------------------------------------------------
	//------------------------------------------------------------------------
	//TUPLE
	
	class Tuple: public Term_derivation_helper<Tuple>
	{
	private:
		Term_sp_vector args;
	public:
		Tuple(void);
		
		Tuple(Term_sp_vector const& args_);
		
		virtual ~Tuple()
		{
		};
		
		virtual void
		set_string(void);
		
		virtual Instance_return
		get_variable_map(Term_sp const& other) const;
		
		virtual bool
		check_instance(Term_sp const& other) const;
		
		virtual Term_sp
		set_member(Instance_variable_map_sp const& map) const;
		
		/*-----------------------------------------------------------------*/
		
		virtual bool
		operator==(Tuple const& other) const;
		
		virtual inline bool
		operator!=(Tuple const& other) const
		{
			return !operator==(other);
		}
		
		virtual inline bool
		operator==(Term const& other) const
		{
			
			return type != other.type ? false : operator==(dynamic_cast<Tuple const&>(other));
		}
		
		virtual inline bool
		operator!=(const Term& other) const
		{
			return !operator==(other);
		}
	};
	
	class Variable: public Term_derivation_helper<Variable>
	{
	private:
		std::string val;
	
	public:
		Variable(std::string const& val_)
			: Term_derivation_helper(Term_type::VAR),
			  val(val_)
		{
			set_string();
		}
		
		virtual ~Variable()
		{
		};
		
		void
		set_string(void)
		{
			print_str = val;
		}
		
		virtual Instance_return
		get_variable_map(Term_sp const& other) const
		{
			auto map = std::make_shared<Instance_variable_map>();
			map->insert(Instance_single_map(std::static_pointer_cast<Variable>(clone()),
			                                other));
			return Instance_return(true,
			                       map);
		}
		
		virtual bool
		check_instance(Term_sp const& other) const
		{
			return true;
		};
		
		virtual Term_sp
		set_member(Instance_variable_map_sp const& map) const
		{
			Variable_sp duplicate = std::static_pointer_cast<Variable>(clone());
			if(map->find(duplicate) == map->end())
			{
				return duplicate;
			}
			else
			{
				return (* map)[duplicate]->clone();
			}
		}
		
		/*-----------------------------------------------------------------*/
		
		virtual bool
		operator==(Variable const& other) const
		{
			return val == other.val;
		}
		
		virtual inline bool
		operator!=(Variable const& other) const
		{
			return !operator==(other);
		}
		
		virtual inline bool
		operator==(Term const& other) const
		{
			return type != other.type ? false : operator==(dynamic_cast<Variable const&>(other));
		}
		
		virtual inline bool
		operator!=(const Term& other) const
		{
			return !operator==(other);
		}
	};
	
	class Identifier: public Term_derivation_helper<Identifier>
	{
	private:
		std::string val;
	
	public:
		
		Identifier(std::string const& val_)
			: Term_derivation_helper(Term_type::IDE),
			  val(val_)
		{
			set_string();
		}
		
		virtual ~Identifier()
		{
		}
		
		void
		set_string(void)
		{
			print_str = val;
		}
		
		virtual Instance_return
		get_variable_map(Term_sp const& other) const
		{
			return Instance_return(operator==(* other),
			                       std::make_shared<Instance_variable_map>());
		}
		
		virtual bool
		check_instance(Term_sp const& other) const
		{
			return operator==(* other);
		};
		
		virtual Term_sp
		set_member(Instance_variable_map_sp const& map) const
		{
			return clone();
		}
		
		/*-----------------------------------------------------------------*/
		
		virtual bool
		operator==(Identifier const& other) const
		{
			return val == other.val;
		}
		
		virtual inline bool
		operator!=(Identifier const& other) const
		{
			return !operator==(other);
		}
		
		virtual inline bool
		operator==(Term const& other) const
		{
			return type != other.type ? false : operator==(dynamic_cast<Identifier const&>(other));
		}
		
		virtual inline bool
		operator!=(const Term& other) const
		{
			return !operator==(other);
		}
	};
	
	class Number: public Term_derivation_helper<Number>
	{
	private:
		std::string val;
	
	public:
		
		Number(std::string const& val_)
			: Term_derivation_helper(Term_type::NUM),
			  val(val_)
		{
			set_string();
		}
		
		virtual ~Number()
		{
		}
		
		void
		set_string(void)
		{
			print_str = val;
		}
		
		virtual inline Instance_return
		get_variable_map(Term_sp const& other) const
		{
			return Instance_return(operator==(* other),
			                       std::make_shared<Instance_variable_map>());
		}
		
		virtual bool
		check_instance(Term_sp const& other) const
		{
			return operator==(* other);
		};
		
		virtual inline Term_sp
		set_member(Instance_variable_map_sp const& map) const
		{
			return clone();
		}
		
		/*-----------------------------------------------------------------*/
		
		virtual bool
		operator==(Number const& other) const
		{
			return val == other.val;
		}
		
		virtual inline bool
		operator!=(Number const& other) const
		{
			return !operator==(other);
		}
		
		virtual inline bool
		operator==(Term const& other) const
		{
			return type != other.type ? false : operator==(dynamic_cast<Number const&>(other));
		}
		
		virtual inline bool
		operator!=(const Term& other) const
		{
			return !operator==(other);
		}
	};
}

//------------------------------------------------------------------
// Hash Functions
namespace std
{
	template<>
	struct hash<ASP_tools::Function_sp>
	{
		size_t
		operator()(ASP_tools::Function_sp const& x) const noexcept
		{
			return (hash<std::string>()(x->to_string()));
		}
	};
	
	template<>
	struct equal_to<ASP_tools::Function_sp>
	{
		bool
		operator()(ASP_tools::Function_sp const& x,
		           ASP_tools::Function_sp const& y) const
		{
			return x->to_string() == y->to_string();
		}
	};
}


#endif //CSOFTWARE_ASP_ATOM_PARSER_H
