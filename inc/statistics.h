//
// Created by faruxx on 06.09.2016.
//

#ifndef HCP_STATISTICS_H_H
#define HCP_STATISTICS_H_H

#include <planner.h>

namespace HCP
{
	class Statistics
	{
	public:
		Statistics(Planner const& planner_)
			: planner(planner_)
		{
			tree_no_sens_act               = 0;
			tree_no_actu_act               = 0;
			tree_no_goals                  = 0;
			tree_no_fails                  = 0;
			tree_avg_depth                 = 0.0;
			tree_max_depth                 = 0;
			tree_avg_sens_depth            = 0.0;
			tree_max_sens_depth            = 0;
			tree_min_sens_depth            = 0;
			tree_avg_goal_depth            = 0.0;
			tree_max_goal_depth            = 0;
			tree_min_goal_depth            = 0;
			tree_avg_fail_depth            = 0.0;
			tree_max_fail_depth            = 0;
			tree_min_fail_depth            = 0;
			tree_avg_branching_factor      = 0.0;
			tree_avg_sens_branching_factor = 0.0;
			
			number_of_threads = planner.get_number_of_threads();
			
			max_size = (value_size + 2) * (number_of_threads + 1);
		};
		
		void
		generate_statistics(std::string file_name,
		                    uint64_t total_program_time,
		                    int argc,
		                    char** argv);
		
		
		uint64_t tree_no_sens_act;
		uint64_t tree_no_actu_act;
		uint64_t tree_no_goals;
		uint64_t tree_no_fails;
		uint64_t tree_no_timeouts;
		double   tree_avg_depth;
		uint64_t tree_max_depth;
		double   tree_avg_sens_depth;
		uint64_t tree_max_sens_depth;
		uint64_t tree_min_sens_depth;
		double   tree_avg_goal_depth;
		uint64_t tree_max_goal_depth;
		uint64_t tree_min_goal_depth;
		double   tree_avg_fail_depth;
		uint64_t tree_max_fail_depth;
		uint64_t tree_min_fail_depth;
		double   tree_avg_branching_factor;
		double   tree_avg_sens_branching_factor;
		
		uint64_t graph_no_sens_act;
		uint64_t graph_no_actu_act;
		uint64_t graph_no_goals;
		uint64_t graph_no_fails;
		uint64_t graph_no_timeouts;
	
	
	private:
		Planner const& planner;
		
		int number_of_threads;
		
		
		std::string
		print_sep(void);
		
		std::string
		print_partial_sep(void);
		
		std::string
		print_title(std::string title);
		
		std::string
		print_val(std::string title,
		          std::string value);
		
		
		std::string
		generate_general_statistics(uint64_t total_program_time);
		
		std::string
		generate_tree_statistics(void);
		
		std::string
		generate_graph_statistics(void);
		
		uint16_t const title_size = 45;
		uint16_t const value_size = 21;
		uint16_t       max_size;
	};
}


#endif //HCP_STATISTICS_H_H
