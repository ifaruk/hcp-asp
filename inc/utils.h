//
// Created by faruxx on 23.08.2016.
//

#ifndef CSOFTWARE_UTILS_H
#define CSOFTWARE_UTILS_H

#include <memory>
#include <string>
#include <sstream>
#include <iomanip>
#include <vector>
#include <algorithm>

#include <asp_tools/asp_tools.h>

namespace HCP
{
	void
	print_line(std::stringstream& sstream,
	           std::string const& name,
	           std::string const& val,
	           int name_size);
	
	struct Exception: public std::exception
	{
		std::string s;
		
		Exception(std::string ss) noexcept
			: s(ss)
		{}
		
		~Exception() throw()
		{} // Updated
		const char*
		what() const throw()
		{return s.c_str();}
	};
	
	struct Failure_quit_exception: public Exception
	{
		Failure_quit_exception()
			: Exception("Terminating...")
		{
		}
	};
	
	struct Parallel_runtime_data
	{
	};
	
	struct Sensing_outcome
	{
		uint32_t               id;
		ASP_tools::Function_sp function;
		
		std::string
		to_string(void)
		{
			std::stringstream ss;
			ss << id << ":" << function->to_string();
			return ss.str();
		}
	};
	
	typedef Sensing_outcome* Sensing_outcome_p;
	typedef std::shared_ptr<Sensing_outcome> Sensing_outcome_sp;
	
	//--------------------------------------------
	
	struct Sensing_action
	{
		uint32_t                        id;
		ASP_tools::Function_sp          action_function;
		std::vector<Sensing_outcome_sp> outcomes; //@warning:Order is important
		
		std::string
		to_string(void)
		{
			std::stringstream ss;
			ss << id << ":" << action_function->to_string() << "[";
			std::string separator = "";
			for(auto    t:outcomes)
			{
				ss << separator << "(" << t->to_string() << ")";
				separator = ",";
			}
			ss << "]";
			return ss.str();
		}
	};
	
	typedef Sensing_action* Sensing_action_p;
	typedef std::shared_ptr<Sensing_action> Sensing_action_sp;
	
	//--------------------------------------------
	
	struct Simplifier
	{
		ASP_tools::Function_sp        simplifier_fluent;
		ASP_tools::Function_sp_vector simplifies;
		
		std::string
		to_string(void)
		{
			std::stringstream ss;
			ss << simplifier_fluent->to_string() << "[";
			std::string separator = "";
			for(auto    t:simplifies)
			{
				ss << separator << "(" << t->to_string() << ")";
				separator = ",";
			}
			ss << "]";
			return ss.str();
		}
	};
	
	typedef Simplifier* Simplifier_p;
	typedef std::shared_ptr<Simplifier> Simplifier_sp;
	
	//--------------------------------------------
	struct Configuration
	{
		bool set_plan_from_beginning;
		bool set_clingo_disable_optimizations;
		bool set_use_libclingo;
		
		bool set_state_invariants;
		
		bool set_incremental;
		
		bool set_debug;
		bool set_graph;
		bool set_save_plan;
		bool set_clingo_warnings;
		
		//		bool set_hcp_consider_visited_states;
		//		///@warning for set_clingo_consider_visited_states to be true set_hcp_consider_visited_states must be true.
		//		bool set_clingo_consider_visited_states;
		
		uint16_t parallelism_amount;
		uint8_t  visited_state_account_level;
		uint32_t clingo_parallel_search_level;
		
		uint32_t    clingo_search_time_window;
		std::string clingo_var_time_min;
		std::string clingo_var_time_max;
		std::string clingo_var_solution_time_min;
		std::string clingo_var_solution_time_max;
		
		uint32_t planner_time_limit;
		uint32_t branch_time_limit;
		uint32_t branch_try_limit;
		
		std::string name;
		std::string working_directory;
		std::string output_directory;
		std::string instance_directory;
		std::string intermediate_data_directory;
		
		std::string              planner_file;
		std::vector<std::string> problem_definition_files;
		std::string              problem_initial_state_file;
		std::string              problem_landmark_state_file;
		std::string              problem_compiled_user_file;
		
		ASP_tools::Variable_sp time_variable;
		ASP_tools::Function_sp time_function;
		ASP_tools::Function_sp goal_function;
		ASP_tools::Function_sp invariant_indicator;
		ASP_tools::Function_sp visited_state_function;
		ASP_tools::Variable_sp visited_state_function_id_var;
		ASP_tools::Variable_sp visited_state_function_weight_var;
		
		std::vector<ASP_tools::Function_sp>              plan_initial_state;
		std::vector<std::vector<ASP_tools::Function_sp>> landmark_state;
		std::vector<ASP_tools::Function_sp>              state_functions;
		std::vector<ASP_tools::Function_sp>              actuation_action_functions;
		std::vector<Sensing_action_sp>                   sensing_action_functions; //@warning:Order is important
		
		std::vector<ASP_tools::Function_sp> functions_to_be_printed_by_clingo;
		
		std::string
		to_string(void);
		
		Configuration()
			: set_plan_from_beginning(false),
			  set_clingo_disable_optimizations(false),
			  set_use_libclingo(false),
			
			  set_state_invariants(false),
			
			  set_incremental(false),
			
			  set_debug(false),
			  set_graph(false),
			  set_save_plan(false),
			  set_clingo_warnings(false),
			
			  parallelism_amount(0),
			  visited_state_account_level(0),
			  clingo_parallel_search_level(1),
			  clingo_search_time_window(1),
			
			  branch_time_limit(UINT32_MAX),
			  branch_try_limit(0),
			  planner_time_limit(UINT32_MAX)
		{
		}
	};
	
	typedef Configuration* Configuration_p;
	typedef std::shared_ptr<Configuration>       Configuration_sp;
	typedef std::shared_ptr<Configuration const> Configuration_const_sp;
	
	std::string
	exec(const char* cmd);
}

#endif //CSOFTWARE_UTILS_H

