//
// Created by faruxx on 31.08.2016.
//

#ifndef HCP_PLAN_H
#define HCP_PLAN_H

#include <string>
#include <algorithm>

#include <asp_tools/asp_tools.h>

namespace HCP
{
	
	struct Edge;
	struct Node;
	
	typedef Edge* Edge_p;
	typedef Node* Node_p;
	
	typedef std::shared_ptr<Edge> Edge_sp;
	typedef std::weak_ptr<Edge>   Edge_wp;
	typedef std::shared_ptr<Node> Node_sp;
	typedef std::weak_ptr<Node>   Node_wp;
	
	enum class Action_type
	{
		NONE, ACTUATION, SENSING,
	};
	
	enum class Node_type
	{
		NONE, DEAD_END, GOAL, TIMED_OUT
	};
	
	struct Node
	{
		
		uint64_t                      branch_id;
		std::string                   label;
		Node_type                     node_type;
		Action_type                   action_type;
		ASP_tools::Function_sp_vector actions;
		uint32_t                      time;
		
		std::vector<Edge_sp> child_edges;
		
		Node()
			: branch_id(0),
			  label(""),
			  node_type(Node_type::NONE),
			  action_type(Action_type::NONE),
			  node_id(0),
			  time(0)
		{
		}
		
		std::string
		to_string(void);
		
		//Will be used only for drawing graph
		std::size_t
		get_node_id(void)
		{
			if(node_id == 0)
			{
				std::string hash_str("");
				hash_str += std::to_string(branch_id);
				hash_str += label;
				hash_str += std::to_string(static_cast<int>(node_type));
				hash_str += std::to_string(static_cast<int>(action_type));
				for(auto f:actions)
				{
					hash_str += f->to_string() + ",";
				}
				node_id = std::hash<std::string>()(hash_str);
			}
			return node_id;
		}
	
	private:
		std::size_t node_id;
	};
	
	struct Edge
	{
		uint64_t    branch_id;
		std::string label;
		Node_wp     n_from;
		Node_sp     n_to;
		uint32_t    time; //or consider as step
		uint32_t    priority;
		
		int64_t weight; //Weight of visited state (which will effect it to be chosen in future plannings. This is only valid when 'Configuration::visited_state_account_level' is >3.
		bool    is_short_cut; //if this state is visited before, edge points to original edge's next action.
		
		ASP_tools::Function_sp_vector state_functions;
		ASP_tools::Function_sp_vector sensing_outcomes;
		
		ASP_tools::Function_sp_vector action_history;
		ASP_tools::Function_sp_vector sensing_outcome_history;
		
		Edge()
			: branch_id(0),
			  label(""),
			  time(-1),
			  priority(0),
			  n_from(Node_sp(nullptr)),
			  n_to(Node_sp(nullptr))
		{
		}
		
		std::string
		to_string(void);
		
		std::size_t const&
		get_state_id(void)
		{
			return state_id;
		}
		
		void
		set_timeless_state_functions(ASP_tools::Function_sp_vector& tsf)
		{
			//TODO calculate state_id with hash, sort first!!
			timeless_state_functions = tsf;
			std::sort(timeless_state_functions.begin(),
			          timeless_state_functions.end(),
			          [](ASP_tools::Function_sp one,
			             ASP_tools::Function_sp other_one)
			          {
				          return one->to_string() < other_one->to_string();
			          });
			set_state_id();
		}
		
		ASP_tools::Function_sp_vector const&
		get_timeless_state_functions(void)
		{
			return timeless_state_functions;
		}
	
	private:
		ASP_tools::Function_sp_vector timeless_state_functions;
		std::size_t                   state_id; //Hashed value generated from timeless_current_state_functions
		
		//TODO @warning std::hash function is not guaranteed to return same value in every run. Should no be serialized
		void
		set_state_id(void)
		{
			std::string hash_str("");
			for(auto    tsf:timeless_state_functions)
			{
				hash_str += tsf->to_string() + ",";
			}
			state_id = std::hash<std::string>()(hash_str);
		}
	};
	
	struct History
	{
		typedef std::vector<ASP_tools::Function_sp_vector> Function_sp_vector_vector;
		typedef std::vector<std::vector<uint32_t>>         Id_vector_vector;
		
		uint32_t start_time;
		uint32_t goal_time;
		
		Function_sp_vector_vector actuation_actions;
		Function_sp_vector_vector sensing_actions;
		Id_vector_vector          sensing_action_ids;
		Function_sp_vector_vector sensing_outcomes;
		Id_vector_vector          sensing_outcome_ids;
		
		Function_sp_vector_vector state_functions;//sf
		Function_sp_vector_vector timeless_isf;
		
		Function_sp_vector_vector goal_fluents;
		Function_sp_vector_vector visited_state_fluents;
		
		History(uint32_t start_time_,
		        uint32_t goal_time_)
			: start_time(start_time_),
			  goal_time(goal_time_)
		{
			//+2 for including time 0 and goal_time
			actuation_actions.resize(goal_time_ + 2);
			
			sensing_actions.resize(goal_time_ + 2);
			sensing_action_ids.resize(goal_time_ + 2);
			sensing_outcomes.resize(goal_time_ + 2);
			sensing_outcome_ids.resize(goal_time_ + 2);
			
			state_functions.resize(goal_time_ + 2);
			timeless_isf.resize(goal_time_ + 2);
			
			goal_fluents.resize(goal_time_ + 2);
			visited_state_fluents.resize(goal_time_ + 2);
		}
		
		std::string
		to_string(void);
	};
	
	typedef std::shared_ptr<History> History_sp;
}

#endif //HCP_PLAN_H

