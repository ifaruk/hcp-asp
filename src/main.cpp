#include <asp_tools.h>
#include <asp_driver.h>
#include <utils.h>
#include <option_printer.h>
#include <planner.h>
#include <statistics.h>

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/local_time_adjustor.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <climits>
#include <iostream>
#include <fstream>
#include <iomanip>      // std::setw
#include <tuple>
#include <thread>

namespace HCP
{
	void
	parse_planner_file(HCP::Configuration_sp conf)
	{
		uint32_t sensing_action_id_count = 0;
		
		std::cout << "Reading Planner File: " + conf->planner_file + "." << std::endl;
		
		namespace bpt = boost::property_tree;
		
		ASP_tools::ASP_driver asp_parser;
		
		bpt::ptree pt;
		bpt::read_xml(conf->planner_file,
		              pt);
		
		{
			try
			{
				auto data = pt.get<std::string>("hcp.time_variable.<xmlattr>.name");
				if(!asp_parser.parse_string(data))
				{
					throw HCP::Exception("Planner file: Time variable " + data + " is not valid.");
				}
				conf->time_variable = std::static_pointer_cast<ASP_tools::Variable>(asp_parser.get_outcome());
			}
			catch(const bpt::ptree_error& e)
			{
				throw Exception("\"time_variable\" field not found.\n" + std::string(e.what()));
			}
		}
		
		{
			try
			{
				auto data = pt.get<std::string>("hcp.time_predicate.<xmlattr>.function");
				if(!asp_parser.parse_string(data))
				{
					throw HCP::Exception("Planner file: Time predicate " + data + " is not valid.");
				}
				conf->time_function = std::static_pointer_cast<ASP_tools::Function>(asp_parser.get_outcome());
			}
			catch(const bpt::ptree_error& e)
			{
				throw Exception("\"time_predicate\" field not found.\n" + std::string(e.what()));
			}
		}
		
		{
			try
			{
				auto data = pt.get<std::string>("hcp.goal_predicate.<xmlattr>.function");
				if(!asp_parser.parse_string(data))
				{
					throw HCP::Exception("Planner file: Goal predicate " + data + " is not valid.");
				}
				conf->goal_function = std::static_pointer_cast<ASP_tools::Function>(asp_parser.get_outcome());
			}
			catch(const bpt::ptree_error& e)
			{
				throw Exception("\"goal_predicate\" field not found.\n" + std::string(e.what()));
			}
		}
		
		if(conf->set_state_invariants)
		{
			try
			{
				auto data = pt.get<std::string>("hcp.invariant_indicator.<xmlattr>.function");
				if(!asp_parser.parse_string(data))
				{
					throw HCP::Exception("Planner file: Invariant indicator" + data + " is not valid.");
				}
				conf->invariant_indicator = std::static_pointer_cast<ASP_tools::Function>(asp_parser.get_outcome());
			}
			catch(const bpt::ptree_error& e)
			{
				throw Exception("\"invariant_indicator\" field not found.\n" + std::string(e.what()));
			}
		}
		
		{
			try
			{
				conf->clingo_var_time_min = pt.get<std::string>("hcp.clingo_time_min_var.<xmlattr>.name");
			}
			catch(const bpt::ptree_error& e)
			{
				throw Exception("\"clingo_time_min_var\" field not found.\n" + std::string(e.what()));
			}
		}
		
		{
			try
			{
				conf->clingo_var_time_max = pt.get<std::string>("hcp.clingo_time_max_var.<xmlattr>.name");
			}
			catch(const bpt::ptree_error& e)
			{
				throw Exception("\"clingo_time_max_var\" field not found.\n" + std::string(e.what()));
			}
		}
		
		//		{
		//			try
		//			{
		//				for(bpt::ptree::value_type& v:pt.get_child("hcp.plan_initial_state"))
		//				{
		//					if(boost::iequals(v.first,
		//					                  "fluent"))
		//					{
		//						auto data = v.second.get<std::string>("<xmlattr>.function");
		//						if(!asp_parser.parse_string(data))
		//						{
		//							throw HCP::Exception("Planner file: State Fluent" + data + " is not valid.");
		//						}
		//						conf->plan_initial_state.push_back(std::static_pointer_cast<ASP_tools::Function>(asp_parser.get_outcome()));
		//					}
		//					else if(boost::iequals(v.first,
		//					                       "<xmlcomment>"))
		//					{
		//					}
		//					else
		//					{
		//						throw HCP::Exception("Planner file: State Fluent member" + v.first + " is not valid.");
		//					}
		//				}
		//			}
		//			catch(const bpt::ptree_error& e)
		//			{
		//				throw Exception("\"plan_initial_state\" field not found.\n" + std::string(e.what()));
		//			}
		//		}
		
		{
			try
			{
				for(bpt::ptree::value_type& v:pt.get_child("hcp.state_fluents"))
				{
					if(boost::iequals(v.first,
					                  "fluent"))
					{
						auto data = v.second.get<std::string>("<xmlattr>.function");
						if(!asp_parser.parse_string(data))
						{
							throw HCP::Exception("Planner file: State Fluent" + data + " is not valid.");
						}
						auto one = std::static_pointer_cast<ASP_tools::Function>(asp_parser.get_outcome());
						conf->state_functions.push_back(one);
						//					conf->state_functions.push_back(one->negative());
					}
					else if(boost::iequals(v.first,
					                       "<xmlcomment>"))
					{
					}
					else
					{
						throw HCP::Exception("Planner file: State Fluent member " + v.first + " is not valid.");
					}
				}
			}
			catch(const bpt::ptree_error& e)
			{
				throw Exception("\"state_functions\" field not found.\n" + std::string(e.what()));
			}
		}
		
		{
			try
			{
				for(bpt::ptree::value_type& v:pt.get_child("hcp.actuation_actions"))
				{
					if(boost::iequals(v.first,
					                  "action"))
					{
						auto data = v.second.get<std::string>("<xmlattr>.function");
						if(!asp_parser.parse_string(data))
						{
							throw HCP::Exception("Planner file: Actuation action" + data + " is not valid.");
						}
						auto one = std::static_pointer_cast<ASP_tools::Function>(asp_parser.get_outcome());
						conf->actuation_action_functions.push_back(one);
						//					conf->actuation_action_functions.push_back(one->negative());
					}
					else
					{
						throw HCP::Exception("Planner file: actuation_actions member" + v.first + " is not valid.");
					}
				}
			}
			catch(const bpt::ptree_error& e)
			{
				throw Exception("\"actuation_actions\" field not found.\n" + std::string(e.what()));
			}
		}
		
		{
			try
			{
				for(bpt::ptree::value_type& v:pt.get_child("hcp.sensing_actions"))
				{
					
					if(boost::iequals(v.first,
					                  "action"))
					{
						//Read Outcomes
						HCP::Sensing_action_sp new_sa = std::make_shared<HCP::Sensing_action>();
						new_sa->id = sensing_action_id_count++;
						
						uint32_t outcome_id_count = 0;
						for(bpt::ptree::value_type& v2:v.second)
						{
							
							//Actions attributes
							if(boost::iequals(v2.first,
							                  "<xmlattr>"))
							{
								//Read Sensing Action Function
								auto data = v2.second.get<std::string>("function");
								if(!asp_parser.parse_string(data))
								{
									throw HCP::Exception("Planner file: Sensing action" + data + " is not valid.");
								}
								new_sa->action_function = std::static_pointer_cast<ASP_tools::Function>(asp_parser.get_outcome());
							}
							
							else if(v2.first == "outcome")
							{
								for(bpt::ptree::value_type& v3:v2.second)
								{
									
									if(boost::iequals(v3.first,
									                  "<xmlattr>"))
									{
										//Read Sensing Action Function
										auto data3 = v3.second.get<std::string>("function");
										if(!asp_parser.parse_string(data3))
										{
											throw HCP::Exception("Planner file: Sensing action function" + data3 + " is not valid.");
										}
										Sensing_outcome_sp new_outcome(std::make_shared<Sensing_outcome>());
										new_outcome->id       = outcome_id_count++;
										new_outcome->function = std::static_pointer_cast<ASP_tools::Function>(asp_parser.get_outcome());
										new_sa->outcomes.push_back(new_outcome);
									}
								}
							}
						}
						conf->sensing_action_functions.push_back(new_sa);
					}
				}
			}
			catch(const bpt::ptree_error& e)
			{
				throw Exception("\"sensing_actions\" field not found.\n" + std::string(e.what()));
			}
		}
		
		std::cout << "Reading Planner File: SUCCESSFUL." << std::endl;
	}
	
	HCP::Configuration_sp
	load_configurations(int argc,
	                    char** argv)
	{
		auto conf = std::make_shared<HCP::Configuration>();
		
		//---------------------------------------------------------------------------------------------------
		//Parsing Program Arguments...
		{
			namespace bpo=boost::program_options;
			namespace bfs=boost::filesystem;
			
			
			std::string                         appName = boost::filesystem::basename(argv[0]);
			bpo::variables_map                  vm;
			bpo::options_description            desc("Options");
			bpo::positional_options_description positional_options;
			
			unsigned concurentThreadsSupported = std::thread::hardware_concurrency();
			
			std::string dt("");
			
			{
				std::stringstream dtss;
				dtss << boost::posix_time::second_clock::universal_time().date() << "_"
				     << boost::posix_time::second_clock::universal_time().time_of_day();
				dt = dtss.str();
				boost::erase_all(dt,
				                 "-");
				boost::erase_all(dt,
				                 ":");
			}
			
			try
			{
				//@formatter:off
				desc.add_options()("h","Print help messages")
													("n",bpo::value<std::string>(&(conf->name)),"Work name")
													("o",bpo::value<std::string>(&(conf->output_directory)),"Output Directory")
													("r",bpo::value<std::string>(&(conf->intermediate_data_directory)),"Intermediate Data Directory")
													("pl",bpo::value<std::string>(&(conf->planner_file))->required(),"Domain insight file (*.dif) ")
													("pd",bpo::value<std::vector<std::string>>(&(conf->problem_definition_files))->required()->multitoken(),"Problem Definition Files (Except Initial State)")
					                ("pi",bpo::value<std::string>(&(conf->problem_initial_state_file))->required(),"Problem Initial State File")
					                ("plm",bpo::value<std::string>(&(conf->problem_landmark_state_file)),"Problem Landmark States File")
					
													("parallel",bpo::value<int>()->implicit_value(concurentThreadsSupported-1),"Additional parallelism besides Main Thread and Worker Thread. Can be at least 1."
															" Otherwise will be set to 1 automatically.")
													("plan_from_beginning","If set, each branching will be planned from beginning. This option MUST be set, if your problem has complete branch-wise constraints and measurements.")
													("clingo_search_time_window",bpo::value<uint32_t>()->implicit_value(1),"If optimizations are active, we search solution in a time window."
															" If it is not there we are increasing time window to next level to see if it is there. If the value is set to 1."
															"Note that in order to get optimal branches, user has to provide optimization functions for plan length. ")

//						              ("clingo_parallel_search_level",bpo::value<uint32_t>()->implicit_value(1),"Determines the number of threads will be used for searching for solution of a branch."
//															" Can not be more than 'parallel' value.")
					
													("use_invariants","Use fluent invariants, this will improve usage of computed solutions.")

													("incremental_mode","Should be set if domain uses <incmode>.")

													("visited_state_level",bpo::value<int>()->implicit_value(0),std::string(std::string("0: Visited states are not considered while planning. (DEFAULT)\n")
																																																	+std::string("1: Visited states are considered in post processing. Smaller tree while preserving optimum branching.\n")
//																																																	+std::string("2: Visited states are informed to Clingo. Minimum tree size while optimum branching sacrificed.\n")
//																																																	+std::string("3: Visited states are weighted according to distance to Goal. Faster Clingo execution while preserving optimum branching")
													                                                                       ).c_str())
													
					                ("pl_time",bpo::value<uint32_t>(),"Planner time limit in seconds.")
					                ("bl_time",bpo::value<uint32_t>(),"Time limit in seconds for calculation of any branch.")
					                ("bl_try",bpo::value<uint32_t>()->implicit_value(UINT32_MAX),"Trial limit for calculation of any branch.")
					
													("debug","Enable Debug Behavior")
													//("graph","Print plan graph. (It may not be possible due to problem size)")
													//("save","Save Plan data")
													("wclingo","Enable Clingo warnings");


		//@formatter:on
				
				
				
				try
				{
					bpo::store(bpo::command_line_parser(argc,
					                                    const_cast<char const* const*>(argv)).options(desc).style(bpo::command_line_style::default_style | bpo::command_line_style::allow_long_disguise).run(),
					           vm);
					bpo::notify(vm);
				}
				catch(bpo::error& e)
				{
					std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
					std::cerr << desc << std::endl;
					throw HCP::Failure_quit_exception();
				}
			}
			catch(std::exception& e)
			{
				std::cerr << "Unhandled Exception reached the top of main: " << e.what() << ", application will now exit"
				          << std::endl;
				throw HCP::Failure_quit_exception();
			};
			
			/** --help option
					 */
			if(vm.count("--h") || vm.size() == 0)
			{
				std::cout << "ASP Conditional Planner Solver" << std::endl << std::endl;
				rad::OptionPrinter::printStandardAppDesc(appName,
				                                         std::cout,
				                                         desc,
				                                         & positional_options);
				
				return conf;
			}
			
			conf->working_directory = bfs::current_path().string();
			
			if(vm.count("n") == 0)
			{
				conf->name = dt;
			}
			
			if(vm.count("o"))
			{
				//Output data directory
				conf->output_directory = bfs::absolute(bfs::path(conf->output_directory)).string();
			}
			else
			{
				conf->output_directory = conf->working_directory;
			}
			
			conf->instance_directory = bfs::path(bfs::path(conf->output_directory) / bfs::path(conf->name)).string();
			
			if(vm.count("r"))
			{
				//Intermediate data directory
				conf->intermediate_data_directory = vm["-r"].as<std::string>();
			}
			else
			{
				auto idd = bfs::path(conf->instance_directory) / bfs::path("intermediate_data");
				conf->intermediate_data_directory = idd.string();
			}
			
			conf->planner_file = bfs::absolute(bfs::path(conf->planner_file)).string();
			
			if(vm.count("pd"))
			{
				for(std::string& t:conf->problem_definition_files)
				{
					t = bfs::absolute(bfs::path(t)).string();
					if(access(t.c_str(),
					          F_OK) == -1)
					{
						std::cerr << "File \"" << t << "\" not found!" << std::endl;
						throw std::runtime_error("File \"" + t + "\"not fount!");
					}
				}
			}
			
			if(vm.count("pi"))
			{
				std::string t = bfs::absolute(bfs::path(conf->problem_initial_state_file)).string();
				if(access(t.c_str(),
				          F_OK) == -1)
				{
					std::cerr << "File \"" << t << "\" not found!" << std::endl;
					throw std::runtime_error("File \"" + t + "\"not fount!");
				}
			}
			
			if(vm.count("plm"))
			{
				std::string t = bfs::absolute(bfs::path(conf->problem_landmark_state_file)).string();
				if(access(t.c_str(),
				          F_OK) == -1)
				{
					std::cerr << "File \"" << t << "\" not found!" << std::endl;
					throw std::runtime_error("File \"" + t + "\"not fount!");
				}
			}
			
			conf->set_plan_from_beginning = vm.count("plan_from_beginning") ? true : false;
			conf->set_use_libclingo       = vm.count("use_libclingo") ? true : false;
			
			conf->set_state_invariants = vm.count("use_invariants") ? true : false;
			
			conf->set_incremental = vm.count("incremental_mode") ? true : false;
			
			conf->set_debug           = vm.count("debug") ? true : false;
			conf->set_graph           = vm.count("graph") ? true : false;
			conf->set_save_plan       = vm.count("save") ? true : false;
			conf->set_clingo_warnings = vm.count("wclingo") ? true : false;
			
			if(!vm.count("parallel"))
			{
				conf->parallelism_amount = 1;
			}
			else
			{
				conf->parallelism_amount = vm["parallel"].as<int>();
				conf->parallelism_amount = std::max(conf->parallelism_amount,
				                                    static_cast<uint16_t>(1));
				if(conf->parallelism_amount > concurentThreadsSupported)
				{
					std::cout << "Set parallel argument is not supported by your system.";
					std::cout << "Parallel parameter is set to " << concurentThreadsSupported << std::endl;
					
					conf->parallelism_amount = concurentThreadsSupported;
				}
			}
			
			if(!vm.count("clingo_search_time_window"))
			{
				conf->clingo_search_time_window = 1;
			}
			else
			{
				conf->clingo_search_time_window = vm["clingo_search_time_window"].as<uint32_t>();
			}
			
			if(!vm.count("clingo_parallel_search_level"))
			{
				conf->clingo_parallel_search_level = 1;
			}
			else
			{
				conf->clingo_parallel_search_level = vm["clingo_parallel_search_level"].as<uint32_t>();
				
				if(conf->clingo_parallel_search_level > conf->parallelism_amount)
				{
					throw Exception("'clingo_parallel_search_level cannot be bigger that parallelism amount.");
				}
			}
			
			if(!vm.count("visited_state_level"))
			{
				conf->visited_state_account_level = 0;
			}
			else
			{
				conf->visited_state_account_level = vm["visited_state_level"].as<int>();
				if(conf->visited_state_account_level < 0 || conf->visited_state_account_level > 1)
				{
					throw Exception("visited_state_level flag cannot have value: \"" + std::to_string(conf->visited_state_account_level) + "\"");
				}
			}
			
			if(!vm.count("pl_time"))
			{
				conf->planner_time_limit = UINT32_MAX;
			}
			else
			{
				conf->planner_time_limit = vm["pl_time"].as<uint32_t>();
			}
			
			if(!vm.count("bl_time"))
			{
				conf->branch_time_limit = UINT32_MAX;
			}
			else
			{
				conf->branch_time_limit = vm["bl_time"].as<uint32_t>();
			}
			
			conf->branch_time_limit = std::min(conf->branch_time_limit,
			                                   conf->planner_time_limit);
			
			if(!vm.count("bl_try"))
			{
				conf->branch_try_limit = 0;
			}
			else
			{
				conf->branch_try_limit = vm["bl_try"].as<uint32_t>();
			}
			
			conf->problem_compiled_user_file = bfs::path(bfs::path(conf->intermediate_data_directory) / bfs::path("user_file.lp")).string();
		}
		
		//Parsing Program Arguments Completed
		//---------------------------------------------------------------------------------------------------
		//Parse Planner File
		
		parse_planner_file(conf);
		
		//Parse Planner File Completed
		//---------------------------------------------------------------------------------------------------
		//Read Initial State File
		
		try
		{
			ASP_tools::ASP_driver asp_parser;
			std::ifstream         initial_state_file(conf->problem_initial_state_file,
			                                         std::ifstream::in);
			
			std::string data;
			std::string all_file_content;
			while(std::getline(initial_state_file,
			                   data))
			{
				data.erase(data.begin(),
				           std::find_if(data.begin(),
				                        data.end(),
				                        std::not1(std::ptr_fun<int,
				                                               int>(std::isspace))));
				if(data[0] != '%')
				{
					all_file_content += data;
				}
			}
			
			std::size_t prev = 0, pos;
			while((pos = all_file_content.find_first_of(".",
			                                            prev)) != std::string::npos)
			{
				if(!asp_parser.parse_string(all_file_content.substr(prev,
				                                                    pos - prev)))
				{
					throw HCP::Exception("Planner file: State Fluent" + all_file_content.substr(prev,
					                                                                            pos - prev) + " is not valid.");
				}
				conf->plan_initial_state.push_back(std::static_pointer_cast<ASP_tools::Function>(asp_parser.get_outcome()));
				prev = pos + 1;
			}
			initial_state_file.close();
		}
		catch(Exception& e)
		{
			std::cerr << "Error on parsing initialization file." << std::endl;
			throw e;
		}
		
		
		//Read Initial State File Completed
		//---------------------------------------------------------------------------------------------------
		//Read Landmark State File
		
		try
		{
			ASP_tools::ASP_driver asp_parser;
			std::ifstream         landmark_state_file(conf->problem_landmark_state_file,
			                                          std::ifstream::in);
			
			std::string data;
			std::string all_file_content;
			bool        new_landmark = true;
			while(std::getline(landmark_state_file,
			                   data))
			{
				data.erase(data.begin(),
				           std::find_if(data.begin(),
				                        data.end(),
				                        std::not1(std::ptr_fun<int,
				                                               int>(std::isspace))));
				if(data.substr(0,
				               3) == "%##")
				{
					//New landmark
					new_landmark = true;
				}
				else if(data[0] == '%')
				{
				
				}
				else
				{
					if(!asp_parser.parse_string(data.substr(0,
					                                        data.find_first_of('.'))))
					{
						throw HCP::Exception("Planner file: State Fluent" + data + " is not valid.");
					}
					if(new_landmark)
					{
						conf->landmark_state.emplace_back();
						new_landmark = false;
					}
					conf->landmark_state.back().push_back(std::static_pointer_cast<ASP_tools::Function>(asp_parser.get_outcome()));
				}
			}
			landmark_state_file.close();
			if(!conf->landmark_state.empty() && conf->landmark_state.back().empty())
			{
				conf->landmark_state.pop_back();
			}
		}
		catch(Exception& e)
		{
			std::cerr << "Error on parsing initialization file." << std::endl;
			throw e;
		}
		
		
		//Read Landmark State File Completed
		//---------------------------------------------------------------------------------------------------
		//Prepare Visited State Functions
		
		{
			using namespace ASP_tools;
			conf->visited_state_function_id_var     = std::make_shared<Variable>("Id");
			conf->visited_state_function_weight_var = std::make_shared<Variable>("Weight");
			conf->visited_state_function            = Function_sp(new Function(std::make_shared<Identifier>("visited_state"),
			                                                                   {std::static_pointer_cast<Term>(conf->visited_state_function_id_var),
			                                                                    std::static_pointer_cast<Term>(conf->visited_state_function_weight_var),
			                                                                    std::static_pointer_cast<Term>(conf->time_variable)}));
		}
		//Prepare Visited State Functions Completed
		//---------------------------------------------------------------------------------------------------
		//Prepare Functions to be Printed by Clingo
		
		{
			//Time and goal function will be printed
			auto& prep_vector = conf->functions_to_be_printed_by_clingo;
			//			prep_vector.push_back(conf->time_function);
			prep_vector.push_back(conf->goal_function);
			if(conf->set_state_invariants)
			{
				prep_vector.push_back(conf->invariant_indicator);
			}
			prep_vector.insert(prep_vector.end(),
			                   conf->state_functions.begin(),
			                   conf->state_functions.end());
			prep_vector.insert(prep_vector.end(),
			                   conf->actuation_action_functions.begin(),
			                   conf->actuation_action_functions.end());
			for(auto sa:conf->sensing_action_functions)
			{
				prep_vector.push_back(sa->action_function);
				
				for(auto out:sa->outcomes)
				{
					prep_vector.push_back(out->function);
				}
			}
			
			//Otherwise it is unnecessary.
			if(conf->visited_state_account_level > 1)
			{
				prep_vector.push_back(conf->visited_state_function);
			}
			
			std::sort(prep_vector.begin(),
			          prep_vector.end(),
			          [](ASP_tools::Function_sp& a,
			             ASP_tools::Function_sp& b)
			          {return a->to_string() < b->to_string();});
		}
		//Prepare Functions to be Printed by Clingo Completed
		//---------------------------------------------------------------------------------------------------
		//Prepare Program Data and Directory
		
		{
			namespace bfs=boost::filesystem;
			
			bfs::create_directories(bfs::path(conf->output_directory));
			
			//If instance directory exists then remove it first.
			auto instance_directory_path = bfs::path(conf->instance_directory);
			if(bfs::is_directory(instance_directory_path))
			{
				bfs::remove_all(instance_directory_path);
			}
			bfs::create_directories(instance_directory_path);
			bfs::create_directories(bfs::path(conf->intermediate_data_directory));
			
			std::ofstream user_file(conf->problem_compiled_user_file,
			                        std::ofstream::out);
			
			for(auto f:conf->problem_definition_files)
			{
				std::ifstream file(f,
				                   std::ifstream::in);
				std::string   line;
				user_file << "%" << f << std::endl << std::endl;
				while(std::getline(file,
				                   line))
				{
					if(line.find("#show") != std::string::npos)
					{
						continue;
					}
					
					if(conf->set_clingo_disable_optimizations)
					{
						//@formatter:off
						if(line.find("#minimize") != std::string::npos ||
								line.find("#maximize") != std::string::npos)
						{
							continue;
						}
						//@formatter:on
					}
					
					user_file << line << std::endl;
				}
				file.close();
				user_file << std::endl << "%" << std::string(100,
				                                             '-') << std::endl;
			}
			
			for(auto t:conf->functions_to_be_printed_by_clingo)
			{
				user_file << "#show " << * t->get_name() << "/" << t->get_args().size() << "." << std::endl;
			}
			user_file << std::endl;
			
			user_file.close();
		}
		
		//Prepare Program Data and Directory Completed
		//---------------------------------------------------------------------------------------------------
		//Clingo file paramters
		conf->clingo_var_solution_time_min = "solution_" + conf->clingo_var_time_min;
		conf->clingo_var_solution_time_max = "solution_" + conf->clingo_var_time_max;
		//
		//---------------------------------------------------------------------------------------------------
		
		
		return conf;//SUCCESS;
	}
	
	void
	unload_configurations(Configuration_sp conf)
	{
		namespace bpo=boost::program_options;
		namespace bfs=boost::filesystem;
		
		if(!conf->set_debug)
		{
			auto int_data_dir = bfs::path(conf->intermediate_data_directory);
			if(bfs::is_directory(int_data_dir))
			{
				bfs::remove_all(int_data_dir);
			}
		}
	}
	
	
	void
	print_branch_computation(Configuration_sp conf,
	                         std::vector<HCP::Planner::BCT> bct,
	                         uint32_t no_threads)
	{
		uint64_t total_execution = bct.size();
		
		uint32_t width = static_cast<uint32_t>(log10((double)total_execution)) + 3;
		
		//Sort vector first in ascending order
		std::sort(bct.begin(),
		          bct.end());
		
		int64_t  start             = INT64_MAX;
		int64_t  end               = INT64_MIN;
		uint64_t cell_represents   = 0;
		uint64_t max_state_fluents = 0;
		uint64_t cur_execution_id  = 1;
		std::for_each(bct.begin(),
		              bct.end(),
		              [&](HCP::Planner::BCT& b)
		              {
			              cell_represents   = std::max(b.time,
			                                           cell_represents);
			              start             = std::min(b.millisecond_from_start,
			                                           start);
			              end               = std::max(b.millisecond_from_start + static_cast<int64_t>(b.time),
			                                           end);
			              max_state_fluents = std::max(b.edge->state_functions.size(),
			                                           max_state_fluents);
		              });
		
		cell_represents /= 200;
		cell_represents            = std::max(cell_represents,
		                                      1ul);
		uint64_t cell_size = (end - start + cell_represents - 1) / cell_represents;
		
		uint64_t** cells = new uint64_t* [no_threads];
		for(int i = 0;
		    i < no_threads;
		    i++)
		{
			cells[i] = new uint64_t[cell_size];
			for(int j = 0;
			    j < cell_size;
			    j++)
			{
				cells[i][j] = 0;
			}
		}
		
		std::for_each(bct.begin(),
		              bct.end(),
		              [&](HCP::Planner::BCT const& b)
		              {
			              for(int64_t cur = (b.millisecond_from_start - start) / cell_represents;
			                  (cur * cell_represents) + start < b.millisecond_from_start + b.time;
			                  cur += 1)
			              {
				              cells[b.thread_id][cur] = b.edge->branch_id;
			              }
		              });
		
		std::ofstream plan_file(conf->instance_directory + "/execution.txt",
		                        std::ofstream::out);
		
		{ // Print execution timeline
			plan_file << "Each unit size:" << cell_represents << "ms" << std::endl << std::endl;
			
			for(int j = 0;
			    j < no_threads;
			    j++)
			{
				
				for(int i = 0;
				    i < cell_size;
				    i++)
				{
					
					plan_file << std::setw(width);
					if(cells[j][i] == 0)
					{
						plan_file << ".";
					}
					else if((i > 0 && cells[j][i - 1] == cells[j][i]) && ((i % 10) != 0))
					{
						plan_file << "_";
					}
					else
					{
						plan_file << cells[j][i];
					}
				}
				plan_file << std::endl;
			}
		}
		
		plan_file << std::endl << std::endl;
		{ //Print states
			struct sort_by_length
			{
				inline bool
				operator()(const HCP::Planner::BCT& a,
				           const HCP::Planner::BCT& b)
				{
					return (a.time > b.time);
				}
			};
			
			std::sort(bct.begin(),
			          bct.end(),
			          sort_by_length());
			
			uint32_t twidth = static_cast<uint32_t>(log10((double)end)) + 3;
			
			
			//@formatter:off
			plan_file << std::setw(twidth) << "st" << " :"
			          << std::setw(twidth) << "len" << " :"
			          << std::setw(twidth) << "unkn" << " :"
			          << std::setw(width) << "id" << " :"<<std::endl;
			//@formatter:on
			for(auto b:bct)
			{
				//@formatter:off
				plan_file << std::setw(twidth) << b.time << " :"
				          << std::setw(twidth) << b.plan_length << " :"
				          << std::setw(twidth) << max_state_fluents - b.edge->state_functions.size() << " :"
				          << std::setw(width) << b.edge->branch_id << " :";
				//@formatter:on
				std::sort(b.edge->state_functions.begin(),
				          b.edge->state_functions.end());
				for(auto s:b.edge->state_functions)
				{
					plan_file << s->to_string() << ";";
				}
				plan_file << std::endl;
			}
		}
		
		plan_file.close();
		
		for(int i = 0;
		    i < no_threads;
		    i++)
		{
			delete[] cells[i];
		}
		delete[] cells;
	}
}

int
main(int argc,
     char** argv)
{
	
	HCP::Configuration_sp conf;
	try
	{
		conf = HCP::load_configurations(argc,
		                                argv);
		
		std::cout << conf->to_string() << std::endl;
		
		HCP::Planner planner(conf);
		HCP::Edge_sp root;
		
		uint64_t all_work_time;
		{
			using clock_type = typename std::conditional<std::chrono::high_resolution_clock::is_steady,
			                                             std::chrono::high_resolution_clock,
			                                             std::chrono::steady_clock>::type;
			
			clock_type::time_point all_work_start = clock_type::now();
			if(planner.start_work())
			{
				root = planner.get_result();
			}
			clock_type::time_point all_work_end = clock_type::now();
			all_work_time = std::chrono::duration_cast<std::chrono::milliseconds>(all_work_end - all_work_start).count();
		}
		
		HCP::print_branch_computation(conf,
		                              planner.get_branch_computation_timing(),
		                              planner.get_number_of_threads());
		
		if(HCP::Planner::Planner_state::PLANNER_STATE_ENDED_TIMEOUT == planner.get_state())
		{
			std::cout << "Time out occurred!!!" << std::endl << std::endl;
		}

		planner.print_plan_tree();
		
		HCP::Statistics statistics(planner);
		statistics.generate_statistics(conf->instance_directory + "/statistics.txt",
		                               all_work_time,
		                               argc,
		                               argv);
		
		HCP::unload_configurations(conf);
	}
	catch(HCP::Failure_quit_exception e)
	{
		std::cout << e.what();
		return -1;
	}
	
	return 0;
}
