//
// Created by faruxx on 06.09.2016.
//
#include <statistics.h>

#include <string>
#include <fstream>
#include <numeric>
#include <functional>
#include <set>

namespace HCP
{
	
	std::string
	Statistics::print_sep(void)
	{
		
		return std::string(title_size,
		                   '=') + "+" + std::string(max_size,
		                                            '=') + "\n";
	}
	
	std::string
	Statistics::print_partial_sep(void)
	{
		
		return std::string(title_size,
		                   '-') + "+" + std::string(max_size,
		                                            '-') + "\n";
	}
	
	std::string
	Statistics::print_title(std::string title)
	{
		std::stringstream output;
		output << std::left << std::setw(title_size) << title << "|" << std::endl;
		return output.str();
	}
	
	std::string
	Statistics::print_val(std::string title,
	                      std::string value)
	{
		std::stringstream output;
		output << std::left << std::setw(title_size) << title << ":" << std::right << std::setw(value_size) << value
		       << std::endl;
		return output.str();
	}
	
	std::string
	Statistics::generate_general_statistics(uint64_t total_program_time)
	{
		
		auto get_time_values_from_planner = [&](std::string title,
		                                        uint64_t all_time,
		                                        std::array<uint64_t,
		                                                   MAX_THREAD_COUNT> const& thread_times,
		                                        std::initializer_list<uint64_t>&& dash_values) -> std::string
		{
			std::stringstream ss;
			ss << std::left << std::setw(title_size) << title << ":" << std::right << std::setw(value_size)
			   << std::to_string(all_time);
			ss << "  [";
			std::string sep("");
			int         thread_count = 0;
			for(auto    t:thread_times)
			{
				ss << sep;
				if(std::find(dash_values.begin(),
				             dash_values.end(),
				             t) == dash_values.end())
				{
					ss << std::right << std::setw(value_size) << std::to_string(t);
				}
				else
				{
					ss << std::right << std::setw(value_size) << "-";
				}
				sep = ",";
				thread_count++;
				if(thread_count >= number_of_threads)
				{
					break;
				}
			}
			ss << "]" << std::endl;
			return ss.str();
		};
		
		Edge_sp     root_edge = planner.get_result();
		std::string output("");
		
		output += print_sep();
		output += print_title("General");
		output += print_sep();
		
		output += print_val("Number of Threads",
		                    std::to_string(number_of_threads));
		output += print_val("Execution time",
		                    std::to_string(total_program_time));
		//		output += print_val("Exp. cumulative time(wrt. thread no)(msec)",
		//		                    std::to_string(total_program_time * number_of_threads));
		//
		//		auto total_time_spent_processing = planner.get_cumulative_time_spend_on_processing();
		//		output += get_time_values_from_planner("Cumulative time on processing (msec)",
		//		                                       total_time_spent_processing,
		//		                                       planner.get_cumulative_time_spend_on_processing_by_thread(),
		//		                                       std::initializer_list<uint64_t>{});
		//
		//		output += print_val("Parallellism Efficiency",
		//		                    std::to_string(double(total_time_spent_processing) / (total_program_time * number_of_threads)));
		//
		//		output += print_val("Speed Up (with " + std::to_string(number_of_threads) + " threads)",
		//		                    std::to_string(double(total_time_spent_processing) / total_program_time));
		
		//		output += print_val("Spent time in waiting for task",
		//		                    std::to_string((total_program_time * number_of_threads) - total_time_spent_processing));
		
		auto time_spent_on_clingo = planner.get_total_clingo_execution_time();
		output += get_time_values_from_planner("Clingo execution time (msec)",
		                                       time_spent_on_clingo,
		                                       planner.get_total_clingo_execution_time_by_thread(),
		                                       std::initializer_list<uint64_t>{});
		
		//		output += print_val("Time spent by HCP",
		//		                    std::to_string(total_time_spent_processing - time_spent_on_clingo));
		
		output += print_partial_sep();
		
		auto total_clingo_execution = planner.get_total_number_of_clingo_execution();
		output += get_time_values_from_planner("Number of Clingo execution",
		                                       total_clingo_execution,
		                                       planner.get_total_number_of_clingo_execution_by_thread(),
		                                       std::initializer_list<uint64_t>{});
		{
			double mean;
			double stdev;
			{
				auto v = planner.get_total_number_of_clingo_execution_by_thread();
				mean   = double(total_clingo_execution) / number_of_threads;
				
				std::vector<double> diff(v.size());
				std::transform(v.begin(),
				               v.begin() + number_of_threads,
				               diff.begin(),
				               [mean](double x)
				               {return x - mean;});
				
				double sq_sum = std::inner_product(diff.begin(),
				                                   diff.end(),
				                                   diff.begin(),
				                                   0.0);
				stdev = std::sqrt(sq_sum / number_of_threads);
			}
			
			output += print_val("Thread Task Distribution Mean",
			                    std::to_string(mean));
			output += print_val("Thread Task Distribution StDev",
			                    std::to_string(stdev));
		}
		
		output += print_partial_sep();
		
		output += get_time_values_from_planner("Min Clingo Time (msec)",
		                                       planner.get_min_clingo_execution_instance_time(),
		                                       planner.get_min_clingo_execution_instance_time_by_thread(),
		                                       std::initializer_list<uint64_t>{static_cast<uint64_t>(-1)});
		
		output += get_time_values_from_planner("Max Clingo Time (msec)",
		                                       planner.get_max_clingo_execution_instance_time(),
		                                       planner.get_max_clingo_execution_instance_time_by_thread(),
		                                       std::initializer_list<uint64_t>{0});
		
		output += print_val("Avg. Clingo Time (msec)",
		                    std::to_string(double(planner.get_total_clingo_execution_time()) / (double(planner.get_total_number_of_clingo_execution()))));
		
		output += print_sep();
		
		return output;
	}
	
	std::string
	Statistics::generate_tree_statistics(void)
	{
		Edge_sp root_edge                              = planner.get_result();
		
		
		//------------------------------------------------------------------------------------------------
		//Traverse tree
		
		std::vector<uint64_t> sens_depths;
		std::vector<uint64_t> actu_depths;
		std::vector<uint64_t> goal_depths;
		std::vector<uint64_t> fail_depths;
		std::vector<uint64_t> timeout_depths;
		
		std::vector<uint64_t>         sens_branching;
		std::vector<uint64_t>         all_branching;
		
		std::function<void(Node_sp,
		                   uint64_t)> recursive_walker = [&recursive_walker,
		                                                  &goal_depths,
		                                                  &fail_depths,
		                                                  &timeout_depths,
		                                                  &actu_depths,
		                                                  &sens_depths,
		                                                  &sens_branching,
		                                                  &all_branching](Node_sp cur_node,
		                                                                  uint64_t depth)
		{
			if(Node_type::GOAL == cur_node->node_type)
			{
				goal_depths.push_back(depth - 1);
			}
			else if(Node_type::DEAD_END == cur_node->node_type)
			{
				fail_depths.push_back(depth - 1);
			}
			else if(Node_type::TIMED_OUT == cur_node->node_type)
			{
				timeout_depths.push_back(depth - 1);
			}
			else if(Node_type::NONE == cur_node->node_type)
			{
				if(Action_type::ACTUATION == cur_node->action_type)
				{
					actu_depths.push_back(depth);
				}
				else if(Action_type::SENSING == cur_node->action_type)
				{
					sens_depths.push_back(depth);
					sens_branching.push_back(cur_node->child_edges.size());
				}
				all_branching.push_back(cur_node->child_edges.size());
			};
			
			for(auto edge:cur_node->child_edges)
			{
				recursive_walker(edge->n_to,
				                 depth + 1);
			}
		};
		
		recursive_walker(root_edge->n_to,
		                 0);
		
		//------------------------------------------------------------------------------------------------
		//Prep data
		
		tree_no_sens_act = sens_depths.size();
		tree_no_actu_act = actu_depths.size();
		tree_no_goals    = goal_depths.size();
		tree_no_fails    = fail_depths.size();
		tree_no_timeouts = timeout_depths.size();
		
		if(tree_no_sens_act > 0 && tree_no_actu_act > 0)
		{
			uint64_t tree_sens_total_depth = std::accumulate(sens_depths.begin(),
			                                                 sens_depths.end(),
			                                                 0);
			tree_max_sens_depth = * std::max_element(sens_depths.begin(),
			                                         sens_depths.end());
			tree_min_sens_depth = * std::min_element(sens_depths.begin(),
			                                         sens_depths.end());
			tree_avg_sens_depth = double(tree_sens_total_depth) / sens_depths.size();
			
			uint64_t tree_actu_total_depth = std::accumulate(actu_depths.begin(),
			                                                 actu_depths.end(),
			                                                 0);
			
			tree_avg_depth = double(tree_sens_total_depth + tree_actu_total_depth) / (sens_depths.size() + actu_depths.size());
			tree_max_depth = std::max(tree_max_sens_depth,
			                          * std::max_element(actu_depths.begin(),
			                                             actu_depths.end()));
		}
		
		if(tree_no_goals > 0)
		{
			tree_max_goal_depth = * std::max_element(goal_depths.begin(),
			                                         goal_depths.end());
			tree_min_goal_depth = * std::min_element(goal_depths.begin(),
			                                         goal_depths.end());
			tree_avg_goal_depth = double(std::accumulate(goal_depths.begin(),
			                                             goal_depths.end(),
			                                             0)) / goal_depths.size();
		}
		else
		{
			tree_max_goal_depth = 0;
			tree_min_goal_depth = 0;
			tree_avg_goal_depth = 0;
		}
		
		if(tree_no_fails > 0)
		{
			tree_max_fail_depth = * std::max_element(fail_depths.begin(),
			                                         fail_depths.end());
			tree_min_fail_depth = * std::min_element(fail_depths.begin(),
			                                         fail_depths.end());
			tree_avg_fail_depth = double(std::accumulate(fail_depths.begin(),
			                                             fail_depths.end(),
			                                             0)) / fail_depths.size();
		}
		else
		{
			tree_max_fail_depth = 0;
			tree_min_fail_depth = 0;
			tree_avg_fail_depth = 0;
		}
		
		tree_avg_sens_branching_factor = std::accumulate(sens_branching.begin(),
		                                                 sens_branching.end(),
		                                                 0.0) / sens_branching.size();
		tree_avg_branching_factor      = std::accumulate(all_branching.begin(),
		                                                 all_branching.end(),
		                                                 0.0) / all_branching.size();
		
		
		//------------------------------------------------------------------------------------------------
		// Prepare output
		
		std::string output("");
		output += print_sep();
		output += print_title("Tree Data");
		output += print_sep();
		
		output += print_val("Tree Size",
		                    std::to_string(tree_no_sens_act + tree_no_actu_act));
		output += print_val("No Sensing Act.",
		                    std::to_string(tree_no_sens_act));
		output += print_val("No Actuation Act.",
		                    std::to_string(tree_no_actu_act));
		output += print_val("No Goals",
		                    std::to_string(tree_no_goals));
		output += print_val("No Failures",
		                    std::to_string(tree_no_fails));
		output += print_val("No Timeouts",
		                    std::to_string(tree_no_timeouts));
		output += print_partial_sep();
		output += print_val("Avg. Depth",
		                    std::to_string(tree_avg_depth));
		output += print_val("Max. Depth",
		                    std::to_string(tree_max_depth));
		output += print_partial_sep();
		output += print_val("Avg. Sensing Depth",
		                    std::to_string(tree_avg_sens_depth));
		output += print_val("Max. Sensing Depth",
		                    std::to_string(tree_max_sens_depth));
		output += print_val("Min. Sensing Depth",
		                    std::to_string(tree_min_sens_depth));
		output += print_partial_sep();
		output += print_val("Avg. Goal Depth",
		                    std::to_string(tree_avg_goal_depth));
		output += print_val("Max. Goal Depth",
		                    std::to_string(tree_max_goal_depth));
		output += print_val("Min. Goal Depth",
		                    std::to_string(tree_min_goal_depth));
		output += print_partial_sep();
		output += print_val("Avg. Failure Depth",
		                    std::to_string(tree_avg_fail_depth));
		output += print_val("Max. Failure Depth",
		                    std::to_string(tree_max_fail_depth));
		output += print_val("Min. Failure Depth",
		                    std::to_string(tree_min_fail_depth));
		output += print_partial_sep();
		output += print_val("Avg. Branching Factor",
		                    std::to_string(tree_avg_branching_factor));
		output += print_val("Avg. Sensing Branching Factor",
		                    std::to_string(tree_avg_sens_branching_factor));
		output += print_sep();
		
		return output;
	}
	
	std::string
	Statistics::generate_graph_statistics(void)
	{
		Edge_sp root_edge                              = planner.get_result();
		
		std::set<Node_p>              sens_no;
		std::set<Node_p>              actu_no;
		std::set<Node_p>              goal_no;
		std::set<Node_p>              fail_no;
		std::set<Node_p>              timeout_no;
		//------------------------------------------------------------------------------------------------
		//Traverse graph
		
		std::function<void(Node_sp,
		                   uint64_t)> recursive_walker = [&recursive_walker,
		                                                  &goal_no,
		                                                  &fail_no,
		                                                  &timeout_no,
		                                                  &actu_no,
		                                                  &sens_no](Node_sp cur_node,
		                                                            uint64_t depth)
		{
			if(Node_type::GOAL == cur_node->node_type)
			{
				goal_no.insert(cur_node.get());
			}
			else if(Node_type::DEAD_END == cur_node->node_type)
			{
				fail_no.insert(cur_node.get());
			}
			else if(Node_type::TIMED_OUT == cur_node->node_type)
			{
				timeout_no.insert(cur_node.get());
			}
			else if(Node_type::NONE == cur_node->node_type)
			{
				if(Action_type::ACTUATION == cur_node->action_type)
				{
					actu_no.insert(cur_node.get());
				}
				else if(Action_type::SENSING == cur_node->action_type)
				{
					sens_no.insert(cur_node.get());
				}
			};
			
			for(auto edge:cur_node->child_edges)
			{
				recursive_walker(edge->n_to,
				                 depth + 1);
			}
		};
		
		recursive_walker(root_edge->n_to,
		                 0);
		
		//------------------------------------------------------------------------------------------------
		//Prep data
		
		graph_no_goals    = goal_no.size();
		graph_no_fails    = fail_no.size();
		graph_no_timeouts = timeout_no.size();
		graph_no_actu_act = actu_no.size();
		graph_no_sens_act = sens_no.size();
		
		//------------------------------------------------------------------------------------------------
		// Prepare output
		
		std::string output("");
		output += print_sep();
		output += print_title("Graph Data");
		output += print_sep();
		
		output += print_val("Graph Size",
		                    std::to_string(graph_no_sens_act + graph_no_actu_act));
		output += print_val("No Sensing Act.",
		                    std::to_string(graph_no_sens_act));
		output += print_val("No Actuation Act.",
		                    std::to_string(graph_no_actu_act));
		output += print_val("No Goals",
		                    std::to_string(graph_no_goals));
		output += print_val("No Failures",
		                    std::to_string(graph_no_fails));
		output += print_val("No Timeouts",
		                    std::to_string(graph_no_timeouts));
		output += print_sep();
		
		return output;
	}
	
	void
	Statistics::generate_statistics(std::string file_name,
	                                uint64_t total_program_time,
	                                int argc,
	                                char** argv)
	{
		
		//-----------------------------------------------------------------------------
		
		std::ofstream statistics_file(file_name,
		                              std::ofstream::out);
		
		statistics_file << "Executed command: " << std::endl;
		for(int i = 0;
		    i < argc;
		    i++)
		{
			statistics_file << * (argv + i) << " ";
		}
		statistics_file << std::endl << std::endl;
		statistics_file << generate_general_statistics(total_program_time) << std::endl;
		statistics_file << generate_tree_statistics() << std::endl;
		statistics_file << generate_graph_statistics() << std::endl;
		
		statistics_file.close();
	}
}