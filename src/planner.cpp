#include <planner.h>

#include <boost/algorithm/string.hpp>
#include <boost/thread/locks.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/graph/adjacency_list.hpp>

#include <iostream>
#include <fstream>
#include <iomanip>
#include <numeric>
#include <chrono>

#include <sys/stat.h>

namespace HCP
{
	const std::string Planner::unsatisfiable("UNSATISFIABLE");
	const std::string Planner::satisfiable("SATISFIABLE");
	const std::string Planner::answer("Answer");
	const std::string Planner::unknown("UNKNOWN");
	
	Planner::Edge_explore::Edge_explore(int32_t depth,
	                                    int32_t priority,
	                                    uint64_t lower_search_bound,
	                                    bool landmark,
	                                    const Edge_sp& parent,
	                                    const Edge_sp& to_explore_edge)
		: depth(depth),
		  priority(priority),
		  lower_search_bound(lower_search_bound),
		  landmark(landmark),
		  parent(parent),
		  to_explore_edge(to_explore_edge)
	{}
	
	Planner::Planner(Configuration_sp& conf_)
		: conf(conf_),
		  planning_completed(false),
		  cur_state(PLANNER_STATE_NONE),
		  working_threads(0),
		  no_main_tasks_running(0),
		  next_branch_id(ATOMIC_VAR_INIT(0)),
		  branch_instance_no_threads(static_cast<uint16_t>(std::floor(conf->parallelism_amount / conf->clingo_parallel_search_level))),
		  clingo_instance_per_branch_no_threads(static_cast<uint16_t>(std::ceil(conf->parallelism_amount / branch_instance_no_threads))),
		//		  clingo_instance_per_branch_adaptive_no_threads(ATOMIC_VAR_INIT(clingo_instance_per_branch_no_threads * 10)),
		  clingo_instance_in_program_max(branch_instance_no_threads * clingo_instance_per_branch_no_threads),
		  clingo_instance_in_program_count(ATOMIC_VAR_INIT(0)) //keeps currently running clingo thread instances
	{
		common_command_head.push_back("clingo");
		//		common_command_head.push_back("--quiet=1");
		if(!conf->set_clingo_warnings && !conf->set_debug)
		{
			common_command_head.push_back("--warn=no-file-included");
			common_command_head.push_back("--warn=no-variable-unbounded");
			common_command_head.push_back("--warn=no-operation-undefined");
			common_command_head.push_back("--warn=no-atom-undefined");
		}
		if(conf->set_clingo_disable_optimizations)
		{
			common_command_head.push_back("--opt-mode=ignore");
		}
		
		if(conf->branch_try_limit != 0)
		{
			common_command_head.push_back("--solve-limit=" + std::to_string(conf->branch_try_limit) + "," + std::to_string(conf->branch_try_limit));
		}
		
		
		//Use clingo executable as computation kernel
		///@note:in future libclingo will be the computation kernel
		solver_execute_function = & Planner::clingo_execute;
	}
	
	bool
	Planner::start_work(void)
	{
		cur_state          = PLANNER_STATE_STARTING;
		planning_completed = false;
		planner_start_time = clock_type::now();
		
		for(int i = 0;
		    i < branch_instance_no_threads;
		    i++)
		{
			thread_vector.push_back(std::thread(& Planner::worker_thread,
			                                    this,
			                                    i));
			thread_id_to_index_map.insert(std::pair<std::thread::id,
			                                        int>(thread_vector.back().get_id(),
			                                             i));
			asp_parser_sp_vector.push_back(std::make_shared<ASP_tools::ASP_driver>());
			
			cumulative_time_spend_on_processing[i] = 0;
			
			total_number_of_clingo_execution[i] = 0;
			total_clingo_execution_time[i]      = 0;
			
			min_clingo_execution_instance_time[i] = -1;
			max_clingo_execution_instance_time[i] = 0;
		}
		
		///@warning this part is heavily depend on "prepare_external_file(Edge_pair const& edge_pair)"
		//Prepare initial problem
		Edge_sp dummy_root_parent = std::make_shared<Edge>();
		
		if(!conf->set_plan_from_beginning)
		{
			dummy_root_parent->state_functions = conf->plan_initial_state;
			dummy_root_parent->n_to            = std::make_shared<Node>();
			dummy_root_parent->time            = 0;
		}
		
		solution_root = std::make_shared<Edge>();
		solution_root->time            = 0;
		solution_root->branch_id       = next_branch_id++;
		solution_root->label           = "Initial";
		solution_root->state_functions = conf->plan_initial_state;
		solution_root->n_from          = Node_sp(nullptr);
		solution_root->n_to            = Node_sp(nullptr);
		///@warning:end of warning
		
		
		//When solution_root edge is posted, working threads will start to execute
		{
			std::unique_lock<std::mutex> lk(task_queue_mutex);
			task_queue.push(std::make_shared<Edge_explore>(0,//depth
			                                               100,//0 is highest priority
			                                               0,//0 is where goal will be searched first
			                                               false,
			                                               dummy_root_parent,
			                                               solution_root));
			cur_state = PLANNER_STATE_RUNNING;
			task_queue_cv.notify_all();
		}
		
		for(auto const& landmark:conf->landmark_state)
		{
			Edge_sp dummy_root_parent = std::make_shared<Edge>();
			dummy_root_parent->state_functions = landmark;
			dummy_root_parent->n_to            = std::make_shared<Node>();
			dummy_root_parent->time            = 0;
			
			solution_root = std::make_shared<Edge>();
			solution_root->time            = 0;
			solution_root->branch_id       = next_branch_id++;
			solution_root->label           = "Landmark";
			solution_root->state_functions = landmark;
			solution_root->n_from          = Node_sp(nullptr);
			solution_root->n_to            = Node_sp(nullptr);
			///@warning:end of warning
			
			
			//When solution_root edge is posted, working threads will start to execute
			{
				std::unique_lock<std::mutex> lk(task_queue_mutex);
				task_queue.push(std::make_shared<Edge_explore>(0,//depth
				                                               100,//0 is highest priority
				                                               0,//0 is where goal will be searched first
				                                               true,
				                                               dummy_root_parent,
				                                               solution_root));
				cur_state = PLANNER_STATE_RUNNING;
				task_queue_cv.notify_all();
			}
		}
		
		
		//Join the threads
		for(auto& t:thread_vector)
		{
			t.join();
		}
		
		if(PLANNER_STATE_ENDED_TIMEOUT == cur_state)
		{
			if(!task_queue.empty())
			{
				auto e = task_queue.top();
				std::cout << "Time out, depth " << e->depth << " not completed!";
			}
			while(!task_queue.empty())
			{
				
				Edge_explore_sp edge_explore = task_queue.top();
				task_queue.pop();
				
				
				//If solution is not found, assign DeadNode to solution_root
				edge_explore->to_explore_edge->n_to            = std::make_shared<Node>();
				edge_explore->to_explore_edge->n_to->node_type = Node_type::TIMED_OUT;
				edge_explore->to_explore_edge->n_to->time      = edge_explore->to_explore_edge->time;
				edge_explore->to_explore_edge->label           = std::string("");
			}
		}
		
		return true;
		//		if(PLANNER_STATE_ENDED_SUCCESSFUL == cur_state)
		//		{
		//			return true;
		//		}
		//		else
		//		{
		//			return false;
		//		}
	}
	
	
	
	//Functions above here are public
	//---------------------------------------------------------------------------------------------
	//---------------------------------------------------------------------------------------------
	//Functions below here are private
	
	
	void
	Planner::worker_thread(int thread_id)
	{
		int tid = thread_id;
		//-----------------------------------------------------------
		
		//Welcome message
		std::stringstream message;
		message << std::setw(5) << thread_id << ":" << "Thread started." << std::endl;
		print_message(message.str());
		
		//------------------------------------------------------------
		//Actual work
		
		//Get task
		Edge_explore_sp edge_explore;
		while(!planning_completed)
		{
			{//Get Next element from queue
				std::unique_lock<std::mutex> task_queue_modify_lock(task_queue_mutex);
				if(task_queue.empty())
				{
					task_queue_cv.wait(task_queue_modify_lock,
					                   [&]()
					                   {return !task_queue.empty() || planning_completed;});
				}
				if(planning_completed)
				{
					break;
				}
				else
				{
					edge_explore = task_queue.top();
					task_queue.pop();
					working_threads++;
					if(!edge_explore->landmark)
					{
						no_main_tasks_running++;
					}
				}
			}
			clock_type::time_point work_start = clock_type::now();
			
			//Notify that task is received
			message.str("");
			message << std::setw(5) << thread_id << ":" << "Processing branch \""
			        << std::to_string(edge_explore->to_explore_edge->branch_id) << "\"." << std::endl;
			print_message(message.str());
			
			//Process the task...
			auto edges_to_expand = branch_planner(edge_explore);
			
			{
				std::unique_lock<std::mutex> lk(task_queue_mutex);
				if(edges_to_expand && !edges_to_expand->empty())
				{
					for(auto const& e:* edges_to_expand)
					{
						task_queue.push(e);
					}
				}
				else
				{
				}
				working_threads--;
				if(!edge_explore->landmark)
				{
					no_main_tasks_running--;
				}
				lk.unlock();
				task_queue_cv.notify_all();
			}
			
			{
				std::unique_lock<std::mutex> lk(task_queue_mutex);
				//Only one (me) is working
				if(!(no_main_tasks_running > 0 || (!task_queue.empty() && !task_queue.top()->landmark)))
				{
					planning_completed = true;
					cur_state          = PLANNER_STATE_ENDED_SUCCESSFUL;
					lk.unlock();
					task_queue_cv.notify_all();
				}
			}
			
			clock_type::time_point work_end = clock_type::now();
			cumulative_time_spend_on_processing[tid] += std::chrono::duration_cast<std::chrono::milliseconds>(work_end - work_start).count();
			
			if(std::chrono::duration_cast<std::chrono::seconds>(work_end - planner_start_time).count() > (conf->planner_time_limit))
			{
				planning_completed = true;
				cur_state          = PLANNER_STATE_ENDED_TIMEOUT;
				std::unique_lock<std::mutex> task_queue_modify_lock(task_queue_mutex);
				task_queue.push(edge_explore);//Put back failed task, required to determine not processed depth
			}
		}
		
		// Goodbye message
		message.str("");
		message << std::setw(5) << thread_id << ":" << "Thread done." << std::endl;
		print_message(message.str());
	}
	
	void
	Planner::print_message(std::string const& message)
	{
		std::lock_guard<std::mutex> lock(print_mutex);
		std::cout << message;
	}
	
	bool
	Planner::print_plan_tree(void)
	{
		std::cout << "Printing Plan..." << std::endl;

		struct Edge
		{
			std::string label;
			std::string color;
		};
		struct Vertex
		{
			std::string id;
			std::string name;
			std::string color;
			std::string style;
			std::string shape;
		};
		
		/**Problem initial step*/
		using Graph = boost::adjacency_list<boost::listS,
		                                    boost::listS,
		                                    boost::directedS,
		                                    Vertex,
		                                    Edge>;
		Graph g;
		
		using G_vertex_desc = boost::graph_traits<Graph>::vertex_descriptor;
		using G_edge_desc = boost::graph_traits<Graph>::edge_descriptor;
		
		std::unordered_map<std::size_t,
		         G_vertex_desc> vertices;

		auto add_v = [this,
									&vertices,
									&g](Node_sp const & n)-> G_vertex_desc
			{
				if(n)
				{
					std::string fillcolor;
					std::string shape;
					fillcolor = "bisque1";
					shape     = "ellipse";
					
					if(Node_type::GOAL==n->node_type)
					{
						fillcolor = "mediumseagreen";
						shape     = "invtriangle";
					}
					else if(Node_type::DEAD_END==n->node_type)
					{
						fillcolor = "darkorchid2";
						shape     = "invtriangle";
					}
					else if(Node_type::TIMED_OUT==n->node_type)
					{
						fillcolor = "cornflowerblue";
						shape     = "invtriangle";
					}
					else if(Action_type::SENSING == n->action_type)
					{
						fillcolor = "firebrick2";
						shape     = "ellipse";
					}
					else if(Action_type::ACTUATION == n->action_type)
					{
						fillcolor = "bisque1";
						shape     = "ellipse";
					}
					
					auto v= boost::add_vertex(Vertex{std::to_string(n->get_node_id()),
																											 n->label,
																											 fillcolor,
																											 "filled",
																											 shape},
																								g);
					vertices[n->get_node_id()]=v;
					return v;
				}
				else
				{
					return 0;
				}
			};

		std::queue<Edge_sp> process;
		process.push(solution_root);

		while(!process.empty())
		{
			auto const & s = process.front();
			process.pop();

			auto v1 = vertices[s->n_to->get_node_id()];
			if(0 == v1)
			{
				v1 = add_v(s->n_to);
			}
			if(0 == v1)
			{
				throw std::logic_error("cannot add node to graph");	
			}

			for(auto const & op:s->n_to->child_edges)
			{
				auto const & label  = op->label;
				auto const & to    = op->n_to;
				if(to)
				{
					auto v2 = vertices[to->get_node_id()];
					if(0==v2)
					{
						v2=add_v(to);
						process.push(op);//push this edge so the child nodes of recently added node will be processed as well
					}
					if(0==v2)
					{
						throw std::logic_error("cannot add node to graph");	
					}

					boost::add_edge(v1,
													v2,
													Edge{label,
															 (label.empty() ? "black" : "red")},
													g);
				}
			}
		}

		
		boost::dynamic_properties dp;
		dp.property("vid",
		            boost::get(& Vertex::id,
		                       g));
		
		dp.property("label",
		            boost::get(& Vertex::name,
		                       g));
		dp.property("style",
		            boost::get(& Vertex::style,
		                       g));
		dp.property("fillcolor",
		            boost::get(& Vertex::color,
		                       g));
		
		dp.property("shape",
		            boost::get(& Vertex::shape,
		                       g));
		
		dp.property("label",
		            boost::get(& Edge::label,
		                       g));
		
		dp.property("color",
		            boost::get(& Edge::color,
		                       g));
		
		std::ofstream dotfile(conf->instance_directory + "/plan.dot",
		                        std::ofstream::out);
		boost::write_graphviz_dp(dotfile,
		                         g,
		                         dp,
		                         std::string("vid"));
		
		dotfile.close();
		std::cout << "Plan Printed." << std::endl;
		//Currently we don't provide printing
	}
	
	std::shared_ptr<std::vector<std::string>>
	Planner::prepare_external_data(Edge_explore_sp const& edge_explore)
	{
		//		std::string   ext_file_name = conf->intermediate_data_directory + "/" + std::to_string(edge_explore->to_explore_edge->branch_id) + "_ext.lp";
		//		std::ofstream ext_file(ext_file_name,
		//													 std::ofstream::out);
		
		std::shared_ptr<std::vector<std::string>> external_data(std::make_shared<std::vector<std::string>>());
		
		if(conf->set_plan_from_beginning)
		{
			//			ext_file << "%Plan Initial State" << std::endl;
			for(auto t:conf->plan_initial_state)
			{
				external_data->push_back(t->to_string() + ".");
				//				ext_file << t->to_string() << "." << std::endl;
			}
			
			//			ext_file << "%Action History" << std::endl;
			for(auto t:edge_explore->to_explore_edge->action_history)
			{
				external_data->push_back(":- not " + t->to_string() + ".");
				//				ext_file << ":- not " << t->to_string() << "." << std::endl;
			}
			
			//			ext_file << "%Current Sensing Outcome" << std::endl;
			for(auto t:edge_explore->to_explore_edge->sensing_outcomes)
			{
				external_data->push_back(":- not " + t->to_string() + ".");
				//				ext_file << ":- not " << t->to_string() << "." << std::endl;
			}
			
			//			ext_file << "%Sensing Outcome History" << std::endl;
			for(auto t:edge_explore->to_explore_edge->sensing_outcome_history)
			{
				external_data->push_back(":- not " + t->to_string() + ".");
				//				ext_file << ":- not " << t->to_string() << "." << std::endl;
			}
		}
		else
		{
			//			ext_file << "%Initial State (root_time-1) " << std::endl;
			if(conf->set_incremental)
			{
				external_data->push_back("#program base.");
			}
			for(auto t:edge_explore->parent->state_functions)
			{
				external_data->push_back(t->to_string() + ".");
				//				ext_file << t->to_string() << "." << std::endl;
			}
			
			if(conf->set_incremental)
			{
				external_data->push_back("#program step(t).");
			}
			
			//			ext_file << "%Initial Action (root_time-1) " << std::endl;
			for(auto t:edge_explore->parent->n_to->actions)
			{
				external_data->push_back(":- not " + t->to_string() + ".");
				//				ext_file << ":- not " << t->to_string() << "." << std::endl;
			}
			
			//			ext_file << "%Desired Outcome (root_time)" << std::endl;
			for(auto t:edge_explore->to_explore_edge->sensing_outcomes)
			{
				external_data->push_back(":- not " + t->to_string() + ".");
				//				ext_file << ":- not " << t->to_string() << "." << std::endl;
			}
		}
		
		/**Clingo is informed above level 2.
		 */
		if(conf->visited_state_account_level >= 2)
		{
			throw Exception("Visited state count > 2 is not active yet!");
		}
		return external_data;
	}
	
	void
	Planner::prepare_executed_command(std::string const branch_id,
	                                  std::string command)
	{
		std::string   exc_command_file_name = conf->intermediate_data_directory + "/" + branch_id + "_command.sh";
		std::ofstream exc_command_file(exc_command_file_name,
		                               std::ofstream::out);
		
		exc_command_file << "#!/usr/bin/env bash" << std::endl;
		exc_command_file << command << std::endl;
		exc_command_file.close();
		
		chmod(exc_command_file_name.c_str(),
		      ALLPERMS);
	}
	
	std::shared_ptr<std::vector<std::string>>
	Planner::clingo_execute(uint64_t const branch_id,
	                        uint16_t const thread_id,
	                        uint32_t const cur_step,
	                        uint32_t const increase_step,
	                        uint32_t const time_min,
	                        std::string const ext_file_name,
	                        uint32_t const time_limit)
	{
		if(0 == time_limit)
		{
			return std::make_shared<std::vector<std::string>>();
		}

		//Construct execute_command
		std::vector<std::string> execute_command(common_command_head);
		
		execute_command.push_back("--const");
		execute_command.push_back(conf->clingo_var_time_min + "=" + std::to_string(time_min));
		execute_command.push_back("--const");
		execute_command.push_back(conf->clingo_var_time_max + "=" + std::to_string(cur_step + increase_step - 1));
		
		execute_command.push_back("--const");
		execute_command.push_back(conf->clingo_var_solution_time_min + "=" + std::to_string(cur_step));
		//		execute_command.push_back("--const");
		//		execute_command.push_back(conf->clingo_var_solution_time_max + "=" + std::to_string(cur_step + increase_step - 1));
		
		execute_command.push_back(conf->problem_compiled_user_file);
		execute_command.push_back(ext_file_name);
		
		if(UINT32_MAX != time_limit)
		{
			//Time limit 0 means there is no time limit in clingo, so we set min time limit to 1.
			execute_command.push_back("--time-limit=" + std::to_string(std::max(static_cast<uint32_t>(1),time_limit)));
		}
		
		std::string              clingo_result;
		std::vector<std::string> clingo_result_lines;
		//This block will find clingo_result_lines
		{
			std::string joined_command = boost::algorithm::join(execute_command,
			                                                    " ");
			
			if(conf->set_debug)
			{
				prepare_executed_command(std::to_string(branch_id) + "_" + std::to_string(thread_id),
				                         joined_command);
			}
			
			clingo_result = exec(joined_command.c_str());
			
			boost::split(clingo_result_lines,
			             clingo_result,
			             boost::is_any_of("\n"),
			             boost::token_compress_on);
		}
		
		std::shared_ptr<std::vector<std::string>>      clingo_atoms(std::make_shared<std::vector<std::string>>());
		bool                                           keyword_found = false;
		//Check if solution was successful
		for(std::vector<std::string>::reverse_iterator line          = clingo_result_lines.rbegin();
		    line != clingo_result_lines.rend();
		    ++line)
		{
			
			if(line->substr(0,
			                unsatisfiable.size()) == unsatisfiable)
			{
				keyword_found = true;
				break;
			}
			else if(line->substr(0,
			                     satisfiable.size()) == satisfiable)
			{
				line++;
				boost::split(* clingo_atoms,
				             * line,
				             boost::is_any_of(" "),
				             boost::token_compress_on);
				keyword_found = true;
				break;
			}
			else if(line->substr(0,
			                     answer.size()) == answer)
			{
				line--;
				boost::split(* clingo_atoms,
				             * line,
				             boost::is_any_of(" "),
				             boost::token_compress_on);
				keyword_found = true;
				break;
			}
			else if(line->substr(0,
			                     unknown.size()) == unknown)
			{
				//							throw HCP::Exception("Problem Definition has problems, problem id:" + branch_id + ".");
				throw HCP::Exception("Problem Definition has problems:" + clingo_result);
			}
		}
		if(!keyword_found)
		{
			throw HCP::Exception("Clingo Execution Problem:" + clingo_result);
		}
		return clingo_atoms;
	}
	
	Planner::Find_plan_return
	Planner::find_plan_with_clingo(Edge_explore_sp const& edge_explore,
	                               std::shared_ptr<std::vector<std::string>>& ext_data)
	{
		//		auto branch_id = std::to_string(edge_explore->to_explore_edge->branch_id);
		//Find ret_val
		auto ret_val = std::make_shared<std::vector<std::string >>();
		
		uint64_t       tid           = thread_id_to_index_map[std::this_thread::get_id()];
		uint16_t const plan_limit    = 500; ///@warning hand coded limit: parametrize!
		uint32_t const increase_step = conf->clingo_search_time_window;
		int const      time_min      = conf->set_plan_from_beginning ? 0 : edge_explore->parent->time;
		
		uint64_t local_total_clingo_execution_time        = 0;
		uint64_t local_total_number_of_clingo_execution   = 0;
		uint64_t local_min_clingo_execution_instance_time = static_cast<uint64_t>(-1);
		uint64_t local_max_clingo_execution_instance_time = 0;
		
		std::string   ext = conf->intermediate_data_directory + "/" + std::to_string(edge_explore->to_explore_edge->branch_id) + "_ext.lp";
		std::ofstream ext_file(ext,
		                       std::ofstream::out);
		for(auto& line:* ext_data)
		{
			ext_file << line << std::endl;
		}
		
		///------------------------------------------------------------------------------------------------
		auto clingo_executer = [&](uint32_t cur_step)
		{
			//Beginning of: Collect timing statistics
			clock_type::time_point clingo_start = clock_type::now();
			
			//----------------------------------------------------------------------------------------------
			//Calculate time_limit for current branch
			uint32_t time_left_planner = UINT32_MAX;
			if(UINT32_MAX != conf->planner_time_limit)
			{
				uint32_t spent_time = std::chrono::duration_cast<std::chrono::seconds>(clingo_start - planner_start_time).count();
				time_left_planner = conf->planner_time_limit > spent_time ? conf->planner_time_limit - spent_time : 0;
			}
			uint32_t time_limit        = std::min(conf->branch_time_limit,
			                                      time_left_planner);
			
			//Clingo call
			//desired 'solver_func' is assigned in constructor
			std::shared_ptr<std::vector<std::string>> clingo_atoms;
			try
			{
				clingo_atoms = (this->*solver_execute_function)(edge_explore->to_explore_edge->branch_id,
				                                                0,
				                                                cur_step,
				                                                increase_step,
				                                                time_min,
				                                                ext,
				                                                time_limit);
			}
			catch(std::exception const& e)
			{
				std::cout << e.what() << std::endl;
				clingo_atoms = std::make_shared<std::vector<std::string>>();
			}
			
			ret_val = clingo_atoms->size() == 0 ? std::shared_ptr<std::vector<std::string >>(nullptr) : clingo_atoms;
			
			//----------------------------------------------------------------------------------------------
			//Statistic Generation
			{
				clock_type::time_point clingo_end        = clock_type::now();
				uint64_t               clingo_spend_time = std::chrono::duration_cast<std::chrono::milliseconds>(clingo_end - clingo_start).count();
				std::unique_lock<std::mutex>(local_timing_mutex);
				local_total_clingo_execution_time += clingo_spend_time;
				local_total_number_of_clingo_execution++;
				local_min_clingo_execution_instance_time = std::min(local_min_clingo_execution_instance_time,
				                                                    clingo_spend_time);
				local_max_clingo_execution_instance_time = std::max(local_max_clingo_execution_instance_time,
				                                                    clingo_spend_time);
			}
			//End of: Collect timing statistics
		};
		
		
		//-------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------
		
		auto                   cur_step              = edge_explore->lower_search_bound;
		clock_type::time_point branch_planning_start = clock_type::now();
		while(cur_step < plan_limit)
		{
			clingo_executer(cur_step);
			cur_step += increase_step;
			
			//First thread with the smallest id value has the answer then we found
			if(ret_val.get() != nullptr)
			{
				break;
			}
		}//while(true) // until you find a ret_val or reach plan_time_limit
		
		total_clingo_execution_time[tid] += local_total_clingo_execution_time;
		total_number_of_clingo_execution[tid] += local_total_number_of_clingo_execution;
		min_clingo_execution_instance_time[tid]      = std::min(min_clingo_execution_instance_time[tid],
		                                                        local_min_clingo_execution_instance_time);
		max_clingo_execution_instance_time[tid]      = std::max(max_clingo_execution_instance_time[tid],
		                                                        local_max_clingo_execution_instance_time);
		
		return std::make_tuple(ret_val,
		                       std::chrono::duration_cast<std::chrono::milliseconds>(branch_planning_start - planner_start_time).count(),
		                       local_total_clingo_execution_time);
	};
	
	History_sp
	Planner::prepare_history(Edge_sp const& root_edge,
	                         std::shared_ptr<std::vector<std::string>> const& atoms_str)
	{
		std::shared_ptr<ASP_tools::ASP_driver> parser = asp_parser_sp_vector[thread_id_to_index_map[std::this_thread::get_id()]];
		if(atoms_str && !atoms_str->empty())
		{
			//Sort atoms_str
			std::sort(atoms_str->begin(),
			          atoms_str->end());
			
			
			// Parse strings to ASP_functions
			// And generate a map from function_name+number_of_arguments to ASP_function
			std::map<std::string,
			         std::vector<ASP_tools::Function_sp>> atoms;
			{
				for(auto atom:* atoms_str)
				{
					try
					{
						parser->parse_string(atom);
					}
					catch(...)
					{
						//						std::cerr << e.what() << std::endl;
						throw HCP::Failure_quit_exception();
					}
					ASP_tools::Function_sp outcome = std::static_pointer_cast<ASP_tools::Function>(parser->get_outcome());
					std::string            key     = * (outcome->get_name()) + std::to_string(outcome->get_no_args());
					atoms[key].push_back(outcome);
				}
			}
			
			uint32_t               max_time = -1;
			ASP_tools::Function_sp min_goal;
			
			// Find max_time to consider in this plan : which is earliest reached goal.
			std::string key = * (conf->goal_function->get_name()) + std::to_string(conf->goal_function->get_no_args());
			if(atoms.find(key) == atoms.end() || atoms[key].size() == 0)
			{
				//Goal has not been reached.
				return History_sp(nullptr);
			}
			std::vector<ASP_tools::Function_sp>& goal_atoms = atoms[key];
			for(auto t = goal_atoms.begin();
			    t != goal_atoms.end();
			    t++)
			{
				ASP_tools::Instance_return ret = conf->goal_function->get_variable_map(* t);
				if(ret.first)
				{
					uint32_t cur_time = std::stoul((* ret.second)[conf->time_variable]->to_string());
					if(max_time > cur_time)
					{
						max_time = cur_time;
						min_goal = * t;
					}
				}
				else
				{
					throw ::HCP::Exception("This should not have happened.");
				}
			}
			if(max_time == static_cast<uint64_t>(-1))
			{
				//This means we could not found a goal atom. Then give error.
				throw ::HCP::Exception("Goal predicated \"" + conf->goal_function->to_string() + "\" could not be found.");
			}
			
			//---------------------------------------------------------------------------------------------------------------------
			//---------------------------------------------------------------------------------------------------------------------
			//State Simplifier
			std::set<std::string> invariant_functions_set;
			if(conf->set_state_invariants)
			{
				std::string key = * (conf->invariant_indicator->get_name()) + std::to_string(conf->invariant_indicator->get_no_args());
				std::vector<ASP_tools::Function_sp>& invariant_functions_list = atoms[key];
				for(auto t = invariant_functions_list.begin();
				    t != invariant_functions_list.end();)
				{
					if(conf->invariant_indicator->check_instance(* t))
					{
						invariant_functions_set.insert(std::static_pointer_cast<ASP_tools::Function>((* t)->get_args()[0])->to_string());
						t = invariant_functions_list.erase(t);
					}
					else
					{
						t++;
					}
				}
			}
			
			//---------------------------------------------------------------------------------------------------------------------
			//---------------------------------------------------------------------------------------------------------------------
			// Location of lambda function is important
			// This function also corrects the timing of fluents (plan_from_start causes a different behavior)
			auto find_members = [&](ASP_tools::Function_sp& temp,
			                        History::Function_sp_vector_vector& to_list,
			                        bool remove_matching_atoms,
			                        History::Function_sp_vector_vector* timeless_params,//if nullptr don't assign anything
			                        History::Id_vector_vector* id_vector, //if nullptr don't assign anything to id vector
			                        uint32_t assign_id)//if nullptr don't assign anything to id vector
			{
				std::vector<ASP_tools::Function_sp>& fatoms = atoms[* (temp->get_name()) + std::to_string(temp->get_no_args())];
				for(auto t = fatoms.begin();
				    t != fatoms.end();)
				{
					ASP_tools::Instance_return ret = temp->get_variable_map(* t);
					if(ret.first)
					{
						uint32_t time = std::stoul((* ret.second)[conf->time_variable]->to_string());
						
						if(time >= root_edge->time && time <= max_time)
						{
							//Put into list
							to_list[time].push_back(* t);
							if(id_vector != nullptr)
							{
								(* id_vector)[time].push_back(assign_id);
							}
							
							if(timeless_params != nullptr)
							{
								if(invariant_functions_set.find((* t)->to_string()) == invariant_functions_set.end())
								{
									(* ret.second)[conf->time_variable] = std::static_pointer_cast<ASP_tools::Term>(std::make_shared<ASP_tools::Variable>("T"));
									ASP_tools::Function_sp timeless = std::static_pointer_cast<ASP_tools::Function>(temp->set_member(ret.second));
									(* timeless_params)[time].push_back(timeless);
								}
								else
								{
									* t;
								}
							}
						}
						
						/**
						 * If we are told to delete matching atoms
						 * Or we are planning from the beginning, but we already know the history between time 0 to root_edge_time.
						 * So if the atoms time is in known region, don't consider it for processing and just delete.
						 */
						if(remove_matching_atoms || time < root_edge->time || time > max_time)
						{
							//Found, deleted. check next.
							//std::vector automatically assign next member to t, so we don't need to increase
							t = fatoms.erase(t);
						}
						else
						{
							//Found not deleted. check next.
							t++;
						}
					}
					else
					{
						//Not found. check next.
						t++;
					}
				}
			};
			
			
			/**--------------------------------------------------------------------------------------------------------------------
			 * @important find_members is used in such an order that
			 *  After each execution maximum number of members from atoms list will be removed.
			 */
			
			//-----------------------------------------------------------------
			// Capture sensing actions
			/** @warning: sensing_action_outcomes must be captured before state_functions, because state_functions can also
			 * include sensing_action_outcomes while searching sensing_action_outcomes we are not removing them. But while
			 * searching state_functions we are removing the found values (which later will cause sensing_action_outcomes
			 * to be not found).
			 */
			
			History_sp history = std::make_shared<History>(root_edge->time,
			                                               max_time);// Including time 0
			
			for(auto const& sens_action:conf->sensing_action_functions)
			{
				find_members(sens_action->action_function,
				             history->sensing_actions,
				             true,
				             nullptr,
				             & (history->sensing_action_ids),
				             sens_action->id);
				//
				//				for(auto const &  sens_outcome:sens_action->outcomes)
				//				{
				//					find_members(sens_outcome->function,
				//					             history->sensing_outcomes,
				//					             false,
				//					             nullptr,
				//					             & (history->sensing_outcome_ids),
				//					             sens_outcome->id);
				//				}
			}
			
			//Find sensing outcomes
			for(uint32_t time = 0;
			    time <= max_time;
			    time++)//osa:occurred sensing action
			{
				if(!history->sensing_actions[time].empty())
				{
					auto const& sai    = history->sensing_actions[time][0];//Get first sensing action since there is only one.
					auto const& sai_id = history->sensing_action_ids[time][0];
					auto const& sa     = conf->sensing_action_functions[sai_id];
					
					ASP_tools::Instance_return ret = sa->action_function->get_variable_map(sai);
					if(ret.first)
					{
						ASP_tools::Instance_variable_map_sp vmsp = ret.second;
						(* vmsp)[conf->time_variable] = std::make_shared<ASP_tools::Number>(std::to_string(time + 1));
						
						for(auto const& so:sa->outcomes)
						{
							auto po = std::static_pointer_cast<ASP_tools::Function>(so->function->set_member(vmsp));
							for(auto const& a:atoms[* (po->get_name()) + std::to_string(po->get_no_args())])
							{
								if(* a == * po)
								{
									history->sensing_outcomes[time + 1].push_back(a);
									history->sensing_outcome_ids[time + 1].push_back(so->id);
								}
							}
						}
					}
				}
			}
			
			
			///Check action occurrence restrictions
			for(auto s = history->sensing_actions.begin(), a = history->actuation_actions.begin(), e = history->sensing_actions.end();
			    s != e;
			    ++s, ++a)
			{
				if(s->size() > 1)
				{
					throw Exception("There can not be more then 1 sensing action at any time.");
				}
				else if(s->size() > 0 && a->size() > 0)
				{
					throw Exception("An actuation action and a sensing action can not occur at the same time");
				}
			}
			
			//			std::for_each(history->sensing_outcomes.begin(),
			//			              history->sensing_outcomes.end(),
			//			              [&](ASP_tools::Function_sp_vector& member)
			//			              {
			//				              if(member.size() > 1)
			//				              {
			//					              throw Exception("There can not be more then 1 sensing outcome at any time.");
			//				              }
			//			              });
			
			//-----------------------------------------------------------------
			// Capture state functions
			for(auto state_function:conf->state_functions)
			{
				History::Function_sp_vector_vector* timeless_state_functions = nullptr;
				if(conf->visited_state_account_level > 0)
				{
					timeless_state_functions = & (history->timeless_isf);
				}
				
				find_members(state_function,
				             history->state_functions,
				             true,
				             timeless_state_functions,
				             nullptr,
				             0);
			}
			
			//-----------------------------------------------------------------
			// Capture actuation actions
			for(auto actuation_action_function:conf->actuation_action_functions)
			{
				find_members(actuation_action_function,
				             history->actuation_actions,
				             true,
				             nullptr,
				             nullptr,
				             0);
			}
			
			//-----------------------------------------------------------------
			// Capture visited state functions if necessary
			/** Level 2 is when clingo is informed about visited_states, thats why we are looking for them
			 * in history.*/
			if(conf->visited_state_account_level >= 2)
			{
				find_members(conf->visited_state_function,
				             history->visited_state_fluents,
				             true,
				             nullptr,
				             nullptr,
				             0);
			}
			
			//-----------------------------------------------------------------
			// Capture goal functions
			find_members(conf->goal_function,
			             history->goal_fluents,
			             true,
			             nullptr,
			             nullptr,
			             0);
			
			//---------------------------------------------------------------------------------------------------------------------
			//---------------------------------------------------------------------------------------------------------------------
			//Debug Document
			if(conf->set_debug)
			{
				std::string history_file_name = conf->intermediate_data_directory + "/" + std::to_string(root_edge->branch_id) + "_hist.lp";
				
				std::ofstream history_file(history_file_name,
				                           std::ofstream::out);
				history_file << history->to_string() << std::endl;
				history_file.close();
			}
			
			return history;
		}
		else
		{//No atoms_str found.
			return History_sp(nullptr);
		}
	}
	
	Planner::Create_path_return
	Planner::create_path(Edge_explore_sp ee_sp,
	                     History_sp history)
	{
		auto                      root_edge        = ee_sp->to_explore_edge;
		Edge_explore_sp_vector_sp edges_to_explore = std::make_shared<Edge_explore_sp_vector>();
		
		//No solution found
		if(history.get() == nullptr)
		{
			//If solution is not found, assign DeadNode to solution_root
			root_edge->n_to            = std::make_shared<Node>();
			root_edge->n_to->node_type = Node_type::DEAD_END;
			root_edge->n_to->time      = root_edge->time;
			root_edge->label           = std::string("");
			edges_to_explore->clear();
			return Create_path_return(false,
			                          edges_to_explore);
		}
		
		Visited_state_map possible_visited_map;
		uint32_t          id_count                 = 0;
		
		root_edge->state_functions = history->state_functions[root_edge->time];
		if(conf->visited_state_account_level > 0)
		{
			root_edge->set_timeless_state_functions(history->timeless_isf[root_edge->time]);
		}
		
		//Iterate over History by steps
		Edge_sp      cur_edge = root_edge;
		for(uint32_t step     = history->start_time;
		    step < history->goal_time;
		    step++)
		{
			
			///----------------------------------------------------
			///CREATE ACTION NODE
			Node_sp new_node = std::make_shared<Node>();
			new_node->branch_id = cur_edge->branch_id;
			cur_edge->n_to      = new_node;
			new_node->time      = cur_edge->time;
			
			if(history->sensing_actions[step].size() > 0)
			{
				new_node->action_type = Action_type::SENSING;
				new_node->actions     = history->sensing_actions[step];
				
				if(history->actuation_actions[step].size() > 0)
				{
					throw Exception("A plan can not have both sensing and actuation actions at the same time step");
				}
			}
			else
			{
				new_node->action_type = Action_type::ACTUATION;
				new_node->actions     = history->actuation_actions[step];
			}
			
			auto generate_label = [](ASP_tools::Function_sp_vector& f_vec) -> std::string
			{
				std::string temp_str("");
				std::string sep("");
				
				for(auto f: f_vec)
				{
					temp_str += sep + f->to_string();
					sep = ",";
				}
				return temp_str;
			};
			new_node->label = generate_label(new_node->actions);
			
			///----------------------------------------------------
			///CREATE STATE EDGE
			//Prepare new_edge (for sensing action : occurred edge)
			//Create edge of current sensing action
			Edge_sp new_edge = std::make_shared<Edge>();
			new_edge->branch_id       = new_node->branch_id;
			new_edge->time            = cur_edge->time + 1; ///This edge belongs to next time step
			new_edge->label           = std::string("");
			new_edge->state_functions = history->state_functions[new_edge->time];
			/**Visited_states has to be computed.*/
			if(conf->visited_state_account_level > 0)
			{
				new_edge->set_timeless_state_functions(history->timeless_isf[new_edge->time]);
			}
			new_edge->n_from = new_node;
			new_node->child_edges.push_back(new_edge);
			
			//Setup sensing action
			new_edge->sensing_outcomes = history->sensing_outcomes[new_edge->time];
			if(!new_edge->sensing_outcomes.empty())
			{
				new_edge->label=new_edge->sensing_outcomes[0]->to_string();
			}

			
			//Setup history
			if(conf->set_plan_from_beginning)
			{
				new_edge->action_history = cur_edge->action_history;
				new_edge->action_history.insert(new_edge->action_history.end(),
				                                new_node->actions.begin(),
				                                new_node->actions.end());
				
				new_edge->sensing_outcome_history = cur_edge->sensing_outcome_history;
				new_edge->sensing_outcome_history.insert(new_edge->sensing_outcome_history.end(),
				                                         cur_edge->sensing_outcomes.begin(),
				                                         cur_edge->sensing_outcomes.end());
			}
			
			if(Action_type::SENSING == new_node->action_type)
			{
				//Find Variable map of current sensing actions
				auto cur_sens_action_map = conf->sensing_action_functions[history->sensing_action_ids[step][0]]->action_function->get_variable_map(history->sensing_actions[step][0]).second;
				///@warning we are mapping sensing action's parameters to possible_outcome, but!!! outcome occurres at next time step (which is current edge 'new_edge's time).
				(* cur_sens_action_map)[conf->time_variable] = std::make_shared<ASP_tools::Number>(std::to_string(new_edge->time));
				
				if(history->sensing_outcome_ids[step + 1].empty())
				{
					throw Exception("Expected outcome for action " + conf->sensing_action_functions[history->sensing_action_ids[step][0]]->to_string() + " at time " + std::to_string(step + 1) + " not found!");
				}
				//There can be only one sensing action and one outcome at a time. So we are only checking index 0.
				auto cur_sens_outcome_temp = conf->sensing_action_functions[history->sensing_action_ids[step][0]]->outcomes[history->sensing_outcome_ids[step + 1][0]];
				
				
				
				//Create edges for all other possible outcomes (without complete information, they will be filled then).
				for(auto sens_outcome:conf->sensing_action_functions[history->sensing_action_ids[step][0]]->outcomes)
				{
					if(sens_outcome->id != cur_sens_outcome_temp->id)
					{
						Edge_sp other_edge = std::make_shared<Edge>();
						other_edge->n_from = new_node;
						new_node->child_edges.push_back(other_edge);
						
						other_edge->branch_id = next_branch_id++;
						other_edge->time      = cur_edge->time + 1;
						
						
						///@important: This is the part where current edge differs from the previous one.
						///Setup sensing action
						///@warning we are mapping sensing action's parameters to possible_outcome, but!!! outcome will occur at next time step.
						auto possible_outcome = std::static_pointer_cast<ASP_tools::Function>(sens_outcome->function->set_member(cur_sens_action_map));
						other_edge->sensing_outcomes.push_back(possible_outcome);
						other_edge->label=possible_outcome->to_string();
						
						//Setup history
						if(conf->set_plan_from_beginning)
						{
							//Identical to occurred edge.
							other_edge->action_history          = new_edge->action_history;
							other_edge->sensing_outcome_history = new_edge->sensing_outcome_history;
						}
						
						///@warning important next line decides the priority of edge
						edges_to_explore->push_back(std::make_shared<Edge_explore>(other_edge->time,
						                                                           ee_sp->priority,
						                                                           history->goal_time, //lower bound to search for other_edge's goal. It is sure that will be bigger then current goal
						                                                           ee_sp->landmark,
						                                                           cur_edge,//parent
						                                                           other_edge));//new edge to_explore
					}
				}
			}
			//At this point, we have created new_node which current_edge points to, and new_edge coming out from new_node.
			
			
			//------------------------------------------------------------------------------------
			//------------------------------------------------------------------------------------
			//Check if calculated state has been met before
			{
				bool state_visited_before = false;
				{
					boost::shared_lock<boost::shared_mutex> vs_lock(visited_states_mutex);
					//Check if this state is really visited before
					if(visited_states.find(new_edge->get_state_id()) != visited_states.end())
					{
						state_visited_before = true;
					}
				}
				if(state_visited_before)
				{
					//Point to action this state was pointing before
					new_edge->n_to = visited_states[new_edge->get_state_id()]->n_to;
					
					//Add possible_visited_map to main visited_states
					{
						//Capture writing lock
						boost::unique_lock<boost::shared_mutex> tsf_lock(visited_states_mutex);
						visited_states.insert(possible_visited_map.begin(),
						                      possible_visited_map.end());
					}
					
					//This edge points to tree already. We have to terminate creating path here.
					return Create_path_return(true,
					                          edges_to_explore);
				}
				else
				{
					//Add this state to possible map. If everything goes well, we will add this map to main one.
					possible_visited_map.insert(std::pair<std::size_t,
					                                      Edge_sp>(new_edge->get_state_id(),
					                                               new_edge));
				}
			}
			//------------------------------------------------------------------------------------
			cur_edge = new_edge;
		}//End of loop moving on discovered branch
		
		/**We are done with path creation and it ended up with Either REFERENCE or GOAL
		 *
		 * Update Jan.17 : Visited state account level is limited to 1,
		 * And second part of this if check (history->visited_state_fluents[history->goal_time].size() == 0) implicitly
		 * will be (must be otherwise there is an algorithmic error) true for visited_state_account_level==1.
		 * Because in level 0 and 1; "history->visited_state_fluents" array is always empty.
		 * (I can replace it with "conf->visited_state_account_level == 1" but I need to be absolutely sure and test this.
		 *
		 * So to sum up, first part (if) of this if-else check is always true.
		 */
		if(conf->visited_state_account_level == 0 || history->visited_state_fluents[history->goal_time].empty())
		{
			cur_edge->n_to            = std::make_shared<Node>();
			cur_edge->n_to->node_type = Node_type::GOAL;
			cur_edge->n_to->time      = cur_edge->time;
			
			//Add possible_visited_map to main visited_states
			if(conf->visited_state_account_level > 0)
			{
				//Capture writing lock
				boost::unique_lock<boost::shared_mutex> tsf_lock(visited_states_mutex);
				visited_states.insert(possible_visited_map.begin(),
				                      possible_visited_map.end());
			}
			
			return Create_path_return(true,
			                          edges_to_explore);
		}
		else if(conf->visited_state_account_level > 0)
		{
			/**Each and every state up to this point is checked if it was discovered (visited) before.
			 * If still there is a visited state in solution then this is a false positive.
			 *
			 * We have to add timeless parameters of this state to timeless_state_function_set so that it
			 * will be used in external_file in next loop, where occurrence of false positive will be prevented.
			 */
			
			{
				//Capture writing lock
				boost::unique_lock<boost::shared_mutex> tsf_lock(timeless_state_function_set_mutex);
				for(auto                                tsf:cur_edge->get_timeless_state_functions())
				{
					timeless_state_function_set.insert(tsf);
				}
			}
			edges_to_explore->clear();
			return Create_path_return(false,
			                          edges_to_explore);
		}
		
		return Create_path_return(false,
		                          edges_to_explore);
	}//End of function create_path
	
	
	Planner::Edge_explore_sp_vector_sp
	Planner::branch_planner(Edge_explore_sp& edge_explore)
	{
		while(true)
		{
			auto ext_data = prepare_external_data(edge_explore);
			auto solution = find_plan_with_clingo(edge_explore,
			                                      ext_data);
			auto history  = prepare_history(edge_explore->to_explore_edge,
			                                std::get<0>(solution));
			auto ret_val  = create_path(edge_explore,
			                            history);
			
			
			
			//---------------------------------------------------------------------
			bool path_found_correctly = std::get<0>(ret_val);
			
			{
				boost::unique_lock<boost::shared_mutex> tsf_lock(bct_mutex);
				branch_computation_timing.push_back({static_cast<uint64_t>(thread_id_to_index_map[std::this_thread::get_id()]),
				                                     std::get<1>(solution),
				                                     std::get<2>(solution),
				                                     path_found_correctly ? history->goal_time - history->start_time + 1 : 0,
				                                     edge_explore->to_explore_edge,
				                                     !path_found_correctly});
			}
			if(path_found_correctly)
			{
				return std::get<1>(ret_val);
			}
			else if(conf->visited_state_account_level >= 2)
			{
				/** Retry, results will be different because 'timeless_state_function_set' is updated with
				 * false positive result, which will prevent it to occurre again.
				 */
				continue;
			}
			else
			{
				std::stringstream message;
				message << "Solution Not Found for branch :" << edge_explore->parent->branch_id << "->"
				        << edge_explore->to_explore_edge->branch_id << std::endl;
				print_message(message.str());
				break;
			}
		}
		
		return Edge_explore_sp_vector_sp(nullptr);
	}
}
