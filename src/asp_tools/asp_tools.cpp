//
// Created by faruxx on 18.08.2016.
//

//
//
#include <functional>
#include <algorithm>
#include <sstream>
#include <iostream>

#include <asp_tools.h>

namespace ASP_tools
{
	
	std::size_t
	Instance_mapping_hasher::operator()(Variable_sp const& a) const
	{
		return std::hash<std::string>()(a->to_string());
	}
	
	
	bool
	Instance_mapping_comparator::operator()(Variable_sp const& a,
	                                        Variable_sp const& b) const
	{
		return a->to_string() == b->to_string();
	}
	
	
	/*---------------------------------------------------------------------------------*/
	
	Term::Term(Term_type type_)
		: type(type_),
		  print_str("")
	{
	}
	
	/*---------------------------------------------------------------------------------*/
	
	Function::Function(Identifier_sp name_,
	                   std::vector<Term_sp> const& args_,
	                   bool neg_)
		: Term_derivation_helper(Term_type::FUN),
		  name(name_),
		  args(args_),
		  neg(neg_)
	{
		set_string();
		name_str = std::make_shared<std::string>((neg ? "-" : "") + name->to_string());
		no_args  = args.size();
	}
	
	Function::Function(Identifier_sp name_,
	                   bool neg_)
		: Term_derivation_helper(Term_type::FUN),
		  name(name_),
		  neg(neg_)
	{
		args.clear();
		set_string();
		name_str = std::make_shared<std::string>((neg ? "-" : "") + name->to_string());
		no_args  = 0;
	}
	
	Term_sp_vector const&
	Function::get_args(void) const
	{
		return args;
	}
	
	uint16_t
	Function::get_no_args(void) const
	{
		return no_args;
	}
	
	void
	Function::set_string(void)
	{
		print_str = std::string("");
		print_str += neg ? "-" : "";
		print_str += name->to_string() + "(";
		if(args.size() > 0)
		{
			print_str += args[0]->to_string();
			if(args.size() > 1)
			{
				std::for_each(args.begin() + 1,
				              args.end(),
				              [&](Term_sp t)
				              {
					              print_str += "," + t->to_string();
				              });
			}
		}
		print_str += ")";
	}
	
	Instance_return
	Function::get_variable_map(Term_sp const& other) const
	{
		if(type != other->type)
		{
			return Instance_return(false,
			                       std::make_shared<Instance_variable_map>());
		}
		auto fother = std::static_pointer_cast<Function>(other);
		if(neg != fother->neg || * name != * (fother->name) || no_args != fother->no_args)
		{
			return Instance_return(false,
			                       std::make_shared<Instance_variable_map>());
		}
		
		Instance_variable_map_sp mapping = std::make_shared<Instance_variable_map>();
		
		for(Term_sp_vector::const_iterator this_args = args.begin(), other_args = fother->args.begin();
		    this_args != args.end() && other_args != fother->args.end();
		    ++this_args, ++other_args)
		{
			auto sub_mapping = (* this_args)->get_variable_map((* other_args));
			if(!sub_mapping.first)
			{
				//Return false
				return Instance_return(false,
				                       std::make_shared<Instance_variable_map>());
			}
			else
			{
				///Check if mapping of current argument is consistent with all previously collected mappings
				//argument is an instance
				Instance_variable_map_sp& sub_mapping_up = sub_mapping.second;
				
				for(Instance_variable_map::iterator iterator = sub_mapping_up->begin();
				    iterator != sub_mapping_up->end();
				    ++iterator)
				{
					if(mapping->find(iterator->first) == mapping->end())
					{
						mapping->insert(std::make_pair(iterator->first,
						                               iterator->second));
					}
					else if(* ((* mapping)[iterator->first]) != * (iterator->second))
					{
						//Return false: There is another value for same variable
						return Instance_return(false,
						                       std::make_shared<Instance_variable_map>());
					}
				}
			}
		}
		return Instance_return(true,
		                       mapping);
	};
	
	bool
	Function::check_instance(Term_sp const& other) const
	{
		if(type != other->type)
		{
			return false;
		}
		auto fother = std::static_pointer_cast<Function>(other);
		if(neg != fother->neg || * name != * (fother->name) || no_args != fother->no_args)
		{
			return false;
		}
		
		for(Term_sp_vector::const_iterator this_args = args.begin(), other_args = fother->args.begin();
		    this_args != args.end() && other_args != fother->args.end();
		    ++this_args, ++other_args)
		{
			if(!(* this_args)->check_instance(* other_args))
			{
				//Return false
				return false;
			}
		}
		return true;
	};
	
	
	
	Term_sp
	Function::set_member(Instance_variable_map_sp const& map) const
	{
		Function_sp duplicate = std::static_pointer_cast<Function>(clone());
		
		duplicate->args.clear();
		
		for(Term_sp_vector::const_iterator it = args.begin();
		    it != args.end();
		    ++it)
		{
			duplicate->args.push_back((* it)->set_member(map));
		}
		duplicate->set_string();
		return std::static_pointer_cast<Term>(duplicate);
	}
	
	
	bool
	Function::operator==(Function const& other) const
	{
		if(neg != other.neg)
		{
			return false;
		}
		
		if(* name != * other.name)
		{
			return false;
		}
		
		if(no_args != other.no_args)
		{
			return false;
		}
		
		auto t1 = args.begin();
		auto t2 = other.args.begin();
		
		while(t1 != args.end())
		{
			if(** t1 != ** t2)
			{
				return false;
			}
			t1++;
			t2++;
		}
		
		return true;
	}
	
	
	/*---------------------------------------------------------------------------------*/
	
	Tuple::Tuple(void)
		: Term_derivation_helper(Term_type::TUP)
	{
		to_string();
	}
	
	Tuple::Tuple(std::vector<Term_sp> const& args_)
		: Term_derivation_helper(Term_type::TUP),
		  args(args_)
	{
	}
	
	void
	Tuple::set_string(void)
	{
		print_str = "(";
		
		if(args.size() > 0)
		{
			print_str += args[0]->to_string();
			
			if(args.size() > 1)
			{
				std::for_each(args.begin() + 1,
				              args.end(),
				              [&](Term_sp t)
				              {
					              print_str += "," + t->to_string();
				              });
			}
		}
		print_str += ")";
	}
	
	Instance_return
	Tuple::get_variable_map(Term_sp const& other) const
	{
		if(type != other->type)
		{
			return Instance_return(false,
			                       std::make_shared<Instance_variable_map>());
		}
		
		auto tother = std::static_pointer_cast<Tuple>(other);
		if(args.size() != tother->args.size())
		{
			return Instance_return(false,
			                       std::make_shared<Instance_variable_map>());
		}
		
		Instance_variable_map_sp mapping = std::make_shared<Instance_variable_map>();
		
		for(Term_sp_vector::const_iterator this_args = args.begin(), other_args = tother->args.begin();
		    this_args != args.end() && other_args != tother->args.end();
		    ++this_args, ++other_args)
		{
			auto sub_mapping = (* this_args)->get_variable_map((* other_args));
			if(!sub_mapping.first)
			{
				//Return false
				return Instance_return(false,
				                       std::make_shared<Instance_variable_map>());
			}
			else
			{
				///Check if mapping of current argument is consistent with all previously collected mappings
				//argument is an instance
				Instance_variable_map_sp& sub_mapping_sp = sub_mapping.second;
				
				for(Instance_variable_map::iterator iterator = sub_mapping_sp->begin();
				    iterator != sub_mapping_sp->end();
				    ++iterator)
				{
					if(mapping->find(iterator->first) == mapping->end())
					{
						mapping->insert(std::make_pair(iterator->first,
						                               iterator->second));
					}
					else if(* ((* mapping)[iterator->first]) != * (iterator->second))
					{
						//Return false: There is another value for same variable
						return Instance_return(false,
						                       std::make_shared<Instance_variable_map>());
					}
				}
			}
		}
		
		return Instance_return(true,
		                       mapping);
	};
	
	bool
	Tuple::check_instance(Term_sp const& other) const
	{
		if(type != other->type)
		{
			return false;
		}
		auto tother = std::static_pointer_cast<Tuple>(other);
		if(args.size() != tother->args.size())
		{
			return false;
		}
		
		for(Term_sp_vector::const_iterator this_args = args.begin(), other_args = tother->args.begin();
		    this_args != args.end() && other_args != tother->args.end();
		    ++this_args, ++other_args)
		{
			if(!(* this_args)->check_instance(* other_args))
			{
				//Return false
				return false;
			}
		}
		return true;
	};
	
	Term_sp
	Tuple::set_member(Instance_variable_map_sp const& map) const
	{
		Tuple_sp duplicate = std::static_pointer_cast<Tuple>(clone());
		
		duplicate->args.clear();
		
		for(Term_sp_vector::const_iterator it = args.begin();
		    it != args.end();
		    ++it)
		{
			duplicate->args.push_back((* it)->set_member(map));
		}
		duplicate->set_string();
		return std::static_pointer_cast<Term>(duplicate);
	}
	
	bool
	Tuple::operator==(Tuple const& other) const
	{
		
		if(args.size() != other.args.size())
		{
			return false;
		}
		
		auto t1 = args.begin();
		auto t2 = other.args.begin();
		
		while(t1 != args.end())
		{
			if(** t1 != ** t2)
			{
				return false;
			}
			t1++;
			t2++;
		}
		
		return true;
	}
}

