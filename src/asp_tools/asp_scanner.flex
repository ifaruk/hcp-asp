%{ /* -*- C++ -*- */
#include <iostream>
#include <cstdlib>
#include <asp_scanner.h>
#include <asp_driver.h>
#include <parser/asp_parser.h>
#include <parser/location.hh>

#define yyterminate() ASP_tools::ASP_parser::make_END(ASP_tools::location());

// This will track current scanner location.
// Action is called when length of the token is known.
#define YY_USER_ACTION m_driver.increaseLocation(yyleng);

%}

/*** Flex Declarations and Options ***/

/* enable c++ scanner class generation */
%option c++

/* change the name of the scanner class. results in "ExampleFlexLexer" */
%option prefix="ASP_"

/* the manual says "somewhat more optimized" */
%option batch

/* enable scanner to generate debug output. disable this for release
 * versions. */
%option debug

%option yyclass="ASP_scanner"

%option nodefault

%option noyywrap


NUMBER     0|([1-9][0-9]*)
IDENTIFIER _*[a-z]['A-Za-z0-9_]*
VARIABLE   _*[A-Z]['A-Za-z0-9_]*
BLANK       [ \t\n]


%%

{BLANK}+        {;}
"+"             {return ASP_parser::make_PLUS(ASP_tools::location());}
"-"             {return ASP_parser::make_MINUS(ASP_tools::location());}
"*"             {return ASP_parser::make_TIMES(ASP_tools::location());}
"/"             {return ASP_parser::make_DIVIDE(ASP_tools::location());}
"\\"             {return ASP_parser::make_MODULO(ASP_tools::location());}
"|"             {return ASP_parser::make_ABSOLUTE(ASP_tools::location());}
"&"             {return ASP_parser::make_BITAND(ASP_tools::location());}
"?"             {return ASP_parser::make_BITOR(ASP_tools::location());}
"^"             {return ASP_parser::make_BITEXOR(ASP_tools::location());}
"~"             {return ASP_parser::make_BITNEG(ASP_tools::location());}
","             {return ASP_parser::make_COMMA(ASP_tools::location());}
"("             {return ASP_parser::make_LPAREN(ASP_tools::location());}
")"             {return ASP_parser::make_RPAREN(ASP_tools::location());}
{NUMBER}        {return ASP_parser::make_NUMBER(std::string(yytext),ASP_tools::location());}
{IDENTIFIER}    {return ASP_parser::make_IDENTIFIER(std::string(yytext),ASP_tools::location());}
{VARIABLE}      {return ASP_parser::make_VARIABLE(std::string(yytext),ASP_tools::location());}

.               {;}
<<EOF>>         {return ASP_parser::make_END(ASP_tools::location());}
%%

/*** Additional Code ***/

namespace ASP_tools {

ASP_scanner::ASP_scanner(ASP_driver& _driver)
	:m_driver(_driver)
{
}

ASP_scanner::~ASP_scanner()
{
}

}
