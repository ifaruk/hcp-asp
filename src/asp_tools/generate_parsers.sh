#!/usr/bin/env bash

echo "Deleting old files."
rm -f parser/*
echo "Creating new ones."
echo "flex..."
flex --c++ -f -L -o parser/asp_scanner.cpp asp_scanner.flex
echo "bison..."
bison asp_parser.ypp -o parser/asp_parser.cpp --defines=parser/asp_parser.h
echo "Cleaning #line directives."
#sed -r --in-place 's/^\s*#line [0-9]+//g;' parser/*
echo "done..."
