//
// Created by faruxx on 24.08.2016.
//

#include <fstream>
#include <sstream>
#include <asp_driver.h>
#include <asp_scanner.h>

namespace ASP_tools
{
	ASP_driver::ASP_driver()
		: m_scanner(* this),
		  m_parser(m_scanner,
		           * this),
		  m_location(0)
	{
	}
	
	bool
	ASP_driver::parse_stream(std::istream& in,
	                         const std::string& sname)
	{
		m_scanner.switch_streams(& in,
		                         NULL);
		return (m_parser.parse() == 0);
	}
	
	
	bool
	ASP_driver::parse_string(const std::string& input,
	                         const std::string& sname)
	{
		std::istringstream iss(input);
		try
		{
			return parse_stream(iss,
			                    sname);
		}
		catch(...)
		{
			std::cerr << "Parsing problem with the string :" << input << std::endl;
			throw;
		}
	}
	
	
	Term_sp
	ASP_driver::get_outcome(void)
	{
		return result;
	}
	
	void
	ASP_driver::error(const class location& l,
	                  const std::string& m)
	{
		std::cerr << l << ": " << m << std::endl;
	}
	
	void
	ASP_driver::clear()
	{
		m_location = 0;
	}
	
	void
	ASP_driver::increaseLocation(unsigned int loc)
	{
		m_location += loc;
		//		std::cout << "increaseLocation(): " << loc << ", total = " << m_location << std::endl;
	}
	
	unsigned int
	ASP_driver::location() const
	{
		return m_location;
	}
}