//
// Created by faruxx on 31.08.2016.
//

#include <plan.h>
#include <utils.h>

namespace HCP
{
	
	std::string
	Node::to_string(void)
	{
		std::stringstream ss;
		ss << "(" << std::to_string(branch_id) << ")" << label << "["
		   << (action_type == Action_type::ACTUATION ? "Act" : "Sens") << "]";
		for(auto edge:child_edges)
		{
			ss << edge->label;
		}
		return ss.str();
	}
	
	std::string
	Edge::to_string(void)
	{
		std::stringstream ss;
		//		ss << "(" << * id << ")" << label <<;
		//		for(auto edge:child_edges)
		//		{
		//			ss<<edge->label;
		//		}
		return ss.str();
	}
	
	std::string
	History::to_string(void)
	{
		int name_size = 30;
		int mpsl      = 100; //multi-line parameter separator lenght
		
		std::stringstream ss;
		
		ss << std::endl;
		ss << std::string(mpsl,
		                  '-') << std::endl;
		ss << std::string(mpsl,
		                  '-') << std::endl;
		ss << "HISTORY" << std::endl;
		ss << "|" << std::endl;
		
		//@formatter:off
		print_line(ss,"Time start",std::to_string(start_time),name_size);
		print_line(ss,"Time end",std::to_string(goal_time),name_size);
		//@formatter:on
		
		auto print_vec_vec = [&](std::string title,
		                         Function_sp_vector_vector& vec_vec,
		                         Id_vector_vector* ids)
		{
			print_line(ss,
			           title,
			           std::string(mpsl,
			                       '-'),
			           name_size);
			uint32_t time = 0;
			
			for(auto FspV:vec_vec)
			{
				if(start_time <= time)
				{
					
					std::string temp_str("[" + std::to_string(time) + "]");
					std::string sep = "";
					uint32_t    loc = 0;
					for(auto    Fsp: FspV)
					{
						temp_str += sep;
						if(ids != nullptr)
						{
							temp_str += std::to_string((* ids)[time][loc]) + ":";
						}
						temp_str += Fsp->to_string();
						sep = ";";
						loc++;
					}
					print_line(ss,
					           "",
					           temp_str,
					           name_size);
				}
				time++;
			}
		};
		
		print_vec_vec("Actions",
		              actuation_actions,
		              nullptr);
		
		print_vec_vec("Sensing Actions",
		              sensing_actions,
		              & (sensing_action_ids));
		
		print_vec_vec("Sensing Outcome",
		              sensing_outcomes,
		              & (sensing_outcome_ids));
		
		print_vec_vec("State Fluents",
		              state_functions,
		              nullptr);
		
		print_vec_vec("State Timeless Fluents",
		              timeless_isf,
		              nullptr);
		
		print_vec_vec("Visited State Fluents",
		              visited_state_fluents,
		              nullptr);
		
		print_vec_vec("Goal Fluents",
		              goal_fluents,
		              nullptr);
		
		return ss.str();
	}
}