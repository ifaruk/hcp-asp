//
// Created by faruxx on 23.08.2016.
//
#include <utils.h>

#include <iostream>
#include <iomanip>
#include <sstream>



namespace HCP
{
	
	void
	print_line(std::stringstream& sstream,
	           std::string const& name,
	           std::string const& val,
	           int name_size)
	{
		sstream << std::setw(name_size) << std::left << name << ":" << std::left << val << std::endl;
	}
	
	std::string
	Configuration::to_string()
	{
		int name_size = 30;
		int mpsl      = 100; //multi-line parameter separator lenght
		
		std::stringstream ss;
		
		ss << std::endl;
		ss << std::string(mpsl,
		                  '-') << std::endl;
		ss << std::string(mpsl,
		                  '-') << std::endl;
		ss << "CONFIGURATION" << std::endl;
		ss << "|" << std::endl;
		
		
		//@formatter:off
		print_line(ss,"Start planning from time 0?",set_plan_from_beginning ? "true" : "false",name_size);
		print_line(ss,"Disable clingo optimizations?",set_clingo_disable_optimizations ? "true" : "false",name_size);
//		print_line(ss,"Use clingo adaptive parallelism?",set_clingo_parallel_adaptive ? "true" : "false",name_size);
//		print_line(ss,"Use libclingo_execute?",set_use_libclingo? "true" : "false",name_size);

		print_line(ss,"Debug set?",set_debug ? "true" : "false",name_size);
		print_line(ss,"Graph set?",set_graph ? "true" : "false",name_size);
		print_line(ss,"Save Plan set?",set_save_plan ? "true" : "false",name_size);
		print_line(ss,"Clingo Warnings set?",set_clingo_warnings? "true" : "false",name_size);

		print_line(ss,"Parallelism",std::to_string(parallelism_amount),name_size);
		print_line(ss,"Visited State Account Level",std::to_string(visited_state_account_level),name_size);
		print_line(ss,"Clingo seach sub-parallelism",std::to_string(clingo_parallel_search_level),name_size);
		print_line(ss,"Clingo Search Time Window",std::to_string(clingo_search_time_window),name_size);

		print_line(ss,"Clingo min time variable",clingo_var_time_min,name_size);
		print_line(ss,"Clingo max time variable",clingo_var_time_max,name_size);
		print_line(ss,"Clingo solution min time variable",clingo_var_solution_time_min,name_size);
		print_line(ss,"Clingo solution max time variable",clingo_var_solution_time_max,name_size);

		print_line(ss,"HCP instance name",name,name_size);
		print_line(ss,"Working Directory",working_directory,name_size);
		print_line(ss,"Output Directory",output_directory,name_size);
		print_line(ss,"Problem Instance Directory",instance_directory,name_size);
		print_line(ss,"Intermediate Data Directory",intermediate_data_directory,name_size);

		print_line(ss,"Planner File",planner_file,name_size);
		print_line(ss,"Problem Definition",std::string(mpsl,'-'),name_size);
		if(problem_definition_files.size()>0)
		{
			for(auto t:problem_definition_files)
			{
				print_line(ss,"",t,name_size);
			}
		}
		print_line(ss,"Compiled User File",problem_compiled_user_file,name_size);

		print_line(ss,"Time Variable",time_variable->to_string(),name_size);
		print_line(ss,"Time Function",time_function->to_string(),name_size);
		print_line(ss,"Goal Function",goal_function->to_string(),name_size);
		if(set_state_invariants)
		{
			print_line(ss,"Invariant Function",invariant_indicator->to_string(),name_size);
		}
		print_line(ss,"Visited State Function",visited_state_function->to_string(),name_size);
		print_line(ss,"Visited State Function ID variable",visited_state_function_id_var->to_string(),name_size);
		print_line(ss,"Visited State Function WEIGHT varaible",visited_state_function_weight_var->to_string(),name_size);

		print_line(ss,"Plan Initial State",std::string(mpsl,'-'),name_size);
		if(plan_initial_state.size()>0)
		{
			for(auto t:plan_initial_state)
			{
				print_line(ss,"",t->to_string(),name_size);
			}
		}
		print_line(ss,"State Functions",std::string(mpsl,'-'),name_size);
		if(state_functions.size()>0)
		{
			for(auto t:state_functions)
			{
				print_line(ss,"",t->to_string(),name_size);
			}
		}
//		print_line(ss,"State Simplifier",std::string(mpsl,'-'),name_size);
//		if(simplifier_functions.size()>0)
//		{
//			for(auto t:simplifier_functions)
//			{
//				print_line(ss,"",t->to_string(),name_size);
//			}
//		}
		print_line(ss,"Actuation actions",std::string(mpsl,'-'),name_size);
		if(actuation_action_functions.size()>0)
		{
			for(auto t:actuation_action_functions)
			{
				print_line(ss,"",t->to_string(),name_size);
			}
		}
		print_line(ss,"Sensing Actions",std::string(mpsl,'-'),name_size);
		if(sensing_action_functions.size()>0)
		{
			for(auto t:sensing_action_functions)
			{
				print_line(ss,"",t->to_string(),name_size);
			}
		}
		print_line(ss,"Print in Clingo",std::string(mpsl,'-'),name_size);
		if(functions_to_be_printed_by_clingo.size()>0)
		{
			for(auto t:functions_to_be_printed_by_clingo)
			{
				print_line(ss,"",t->to_string(),name_size);
			}
		}

		//@formatter:on
		return ss.str();
	}
	
	
	std::string
	exec(const char* cmd)
	{
		char                  buffer[128];
		std::string           result = "";
		std::shared_ptr<FILE> pipe(popen(cmd,
		                                 "r"),
		                           pclose);
		if(!pipe)
		{
			throw std::runtime_error("popen() failed!");
		}
		while(!feof(pipe.get()))
		{
			if(fgets(buffer,
			         128,
			         pipe.get()) != NULL)
			{
				result += buffer;
			}
		}
		return result;
	}
}