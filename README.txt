HCP-ASP is a parallel offline algorithm for computing hybrid conditional plans. HCP-ASP is oriented towards robotics applications. The planner relies on modeling actuation actions and sensing actions in answer set programming (ASP), and computation of the branches of a conditional plan in parallel using an ASP solver.

In particular, thanks to external atoms, continuous feasibility checks (like collision checks) are embedded into formal representations of actuation actions and sensing actions in ASP; and thus each branch of a hybrid conditional plan describes a feasible execution of actions to reach their goals. Utilizing nonmonotonic constructs and nondeterministic choices, partial knowledge about states and nondeterministic effects of sensing actions can be explicitly formalized in ASP; and thus each branch of a conditional plan can be computed by an ASP solver without necessitating a conformant planner and an ordering of sensing actions in advance.

More details about the algorithm and its comparison with other conditional planners can be found in the following manuscript: HCP-ASP: Hybrid Conditional Planning using Answer Set Programming

The source code of the planner will be made available soon.

The ASP formulations of the colorball domain and the doors domain used as benchmarks in our experiments can be found here.
