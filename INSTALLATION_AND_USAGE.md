#HCP-ASP
Hybrid Conditional Planner using ASP language.

##Requirements
* clingo 4.5
* g++-5
* flex 2.6
* bison 3.0.4
* boost v1.54 [program_options, filesystem, regex, thread, graph, system]

##Building Steps
mkdir build && cd build
cmake .. 
make
make install

##Program Parameters
```bash
Options:
  --h                                   Print this help messages

  --n arg                               Work name
					                              This name will be used in output files.

  --o arg                               Output Directory
					                              Where the ouput files will be located.
					                              If not given it is set to working directory of binary.

  --r arg                               Intermediate Data Directory
					                              Where the intermediate files will be located.
					                              Many number of intermediate files are generated and 
						                             destroyed during process. If available, chose a
						                             directory reciding in a high speed drive such as SSD.
                                        If --debug flag is present, these files will not be 
                                         removed and can be inspected.

  --pl arg                              Domain insight file (*.dif) 
                                        Holds necessary information required by HCP-ASP to interpret
                                         and use specific domain.

  --pd arg                              Problem Definition Files (Except Initial State)
                                        Domain files (.lp) are given after this flag.
                                        Can be used multiple times in any order.
                                        Multiple files can be listed after each flag.

  --pi arg                              Problem Initial State File
                                        Special file to indicate initial state fluents.

  --parallel [=arg(=7)]                 Additional parallelism besides Main 
                                        Thread and a Worker Thread. 
                                        The value is in between [1,Max Cores of System].
                                        Any value outside of this range will be truncated.

  --plan_from_beginning                 If set, each branching will be planned from beginning. 
                                        This option MUST be set, if your problem has 
                                         branch-size (root-to-leaf) constraints and measurements.
                                        Otherwise plans are computed from already computed intermediate
                                         nodes. (intermediate node-to-leaf).
                                        E.g. A rule stating if a single solution size (root-a leaf) exceeds 
                                         a certain number N.


  --clingo_search_time_window [=arg(=1)]
                                        If optimizations are active, we search 
                                        solution in a time window. If it is not
                                        there we are increasing time window to 
                                        next level to see if it is there. If 
                                        the value is set to 1.Note that in 
                                        order to get optimal branches, user has
                                        to provide optimization functions for 
                                        plan length. 
                                        Mutually Exclusive with --incremental_mode.

  --use_invariants                      Use fluent invariants, this will 
                                        improve usage of computed solutions.
                                        Used to compute equivalence classes.
                                        MUST be with '--visited_state_level 1'

  --incremental_mode                    Should be set if domain uses <incmode>.
                                        MUST when domains are using clingo's incremental mode.

  --visited_state_level [=arg(=0)]      0: Visited states are not considered 
                                        while planning. (DEFAULT)
                                        A state is re-computed if encountered again..

                                        1: Visited states are considered in 
                                        post processing. Smaller tree while 
                                        preserving optimum branching.
                                        A state is compared with already computed states
                                         to find if a solution for it is already found.
                                         Then use already computed state.
                                        NECESSARY fo --use_invariants
                                        
  --pl_time [=arg(=4294967295)]         Planner time limit in seconds.
                                        Overall planner time limit.
                                        If exceeded computed part will be given 
                                         in output.

  --bl_time [=arg(=4294967295)]         Time limit in seconds for calculation 
                                        of any branch.
                                        If exceeded clingo return without a solution.
                                        Passed to clingo's '--time-limit'

  --bl_try [=arg(=4294967295)]          Trial limit for calculation of any branch.
                                        Passed to clingo's '--solve-limit'

  --debug                               Enable Debug Behavior
                                        Intermediate files will not be cleaned after execution.
                                        Additional files will be generated for better insight
                                         of execution.

  --wclingo                             Enable Clingo warnings
                                        All clingo warnings will be printed during execution.
```

